import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { SocketState } from './socket.model';
import * as storage from '../../../storage';


export function createInitialState(): SocketState {
  return {
   connected: false,
   ...storage.getSocket()
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'socket' })
export class SocketStore extends Store<SocketState> {

  constructor() {
    super(createInitialState());
  }

  connect(socket: SocketState) {
    this.update(socket);
    storage.saveSocket(socket);
  }

  get(){
    return storage.getSocket();
  }

  updateConnected(socket: SocketState) {
    this.update(socket);
    storage.saveSocket(socket);
  }

  disconnect() {
    storage.clearSocket();
    this.update(createInitialState());
  }

}

