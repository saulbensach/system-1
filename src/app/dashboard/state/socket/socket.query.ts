import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { SocketStore} from './socket.store';
import { SocketState } from './socket.model';

@Injectable({ providedIn: 'root' })
export class SocketQuery extends Query<SocketState> {
  isConnected$ = this.select(state => state.connected);

  constructor(protected store: SocketStore) {
    super(store);
  }

}
