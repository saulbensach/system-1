import { Injectable, isDevMode } from '@angular/core';
import { Socket } from 'phoenix-channels';
import { SessionQuery } from '../session';
import { environment } from '../../../../environments/environment';
import { SocketStore } from './socket.store';
import { environmentProd } from 'src/environments/environment.prod';

@Injectable({ providedIn: 'root' })
export class SocketService {

  socket: any;
  error: boolean;
  baseUrl: string;

  constructor(
    private sessionQuery: SessionQuery,
    private socketStore: SocketStore
  ) {
    this.sessionQuery.token$.subscribe(token => {
      if (token != null) {
        if(isDevMode()){
          this.baseUrl = environment.socket_url;
        } else {
          this.baseUrl = environmentProd.socket_url;
        }
        this.socket = new Socket(this.baseUrl, {
          params: {token: token},
          transport: WebSocket
        });
        this.socket.onError(error => {
          this.error = true;
        });
        this.socket.onOpen(() => {
          if(this.error == true) {
            this.error = false;
            //window.location.reload();
          }
        });
        this.disconnect()
        this.socket.connect();
        this.socketStore.connect({connected: true});
      } else {
        if(this.socket != null){
          this.disconnect();
        }
      }
    });
    
  }
  
  // Recorrer todos los channels y desconectarlos
  disconnect(){
    
    if(this.socket != null) {
      this.socket.disconnect();
      this.socketStore.disconnect();
    }
  }

  connectTo(channel: string, params?: any){
    return this.socket.channel(channel, {params});
  }

}
