import { Injectable, isDevMode } from '@angular/core';
import { DashboardStore } from './dashboard.store';
import { SessionQuery, SessionService } from '../session';
import { Presence } from 'phoenix-channels';
import { SocketQuery, SocketService } from '../socket';
import { Subscription, Observable, of, Subject } from 'rxjs';
import { VersusStore, VersusQuery } from '../../versus/state';
import { LobbyService } from '../../versus/lobby/state';
import { MdcDialog, MdcDialogRef, MdcSnackbar } from '@angular-mdc/web';
import { ToastrService, GlobalConfig } from 'ngx-toastr';
import { InviteComponent } from 'src/app/shared/customs-toastrs/invite/invite.component';
import { InviteToastrService } from 'src/app/shared/customs-toastrs/invite/state';
import { QuestionaryComponent } from 'src/app/shared/dialogs/questionary/questionary.component';
import { Player } from '../../versus/state/player.model';
import { Team } from '../../versus/state/team.model';
import { AskherComponent } from 'src/app/shared/dialogs/askher/askher.component';
import { FriendRequestComponent } from 'src/app/shared/customs-toastrs/friend-request/friend-request.component';
import { Friend } from 'src/app/models/friend.model';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class DashboardService {

  fileChangeEvent: Observable<any>;
  options: GlobalConfig;
  userChannel: any;
  notifications: any;
  dashboardChannel: any;
  channelLoaded: boolean;
  wannaRef: MdcDialogRef<AskherComponent>;
  teamChannel: any;
  presences: any;
  subscription: Subscription;
  params: any;

  /** TODO hasy un canal extra dashboard: ver que hacer con los casos de error */

  constructor(
    private dashboardStore: DashboardStore,
    private socketService: SocketService,
    private sessionQuery: SessionQuery,
    private socketQuery: SocketQuery,
    private lobbyService: LobbyService,
    private versusStore: VersusStore,
    private versusQuery: VersusQuery,
    private sessionService: SessionService,
    private toastr: ToastrService,
    private inviteTostrSerivce: InviteToastrService,
    private dialog: MdcDialog,
    private router: Router,
    private snackbar: MdcSnackbar
  ) {
    this.channelLoaded = false;
    this.options = this.toastr.toastrConfig;
    this.fileChangeEvent = new Observable();
    this.notifications = [];
    this.presences = {};
    this.subscription = this.sessionQuery.id$.subscribe(id => {
      if(id != null){
        this.socketQuery.isConnected$.subscribe(isConnected => {
          if(isConnected){
            this.versusQuery.team$.subscribe(team => {
              if(team != null){
                if(team.code != null){
                  /*this.teamChannel = this.socketService.connectTo(`team:${team.code}`)
                  this.teamChannel.join().receive("ok", response => {
                    
                  })*/
                }
              }
            });
            this.dashboardChannel = this.socketService.connectTo('dashboard');
            this.userChannel = this.socketService.connectTo(`user:${id}`);
            this.dashboardCallbacks();
          } else {  
            //Está logueado pero el socket no se ha podido conectar
          }
        });
      } else {
        if(this.userChannel != null){
          this.userChannel.push("disconnect", null);
          this.subscription.unsubscribe();
          this.userChannel.leave();
        }
        // Si ha estado logueado y luego se reconecta hay que reiniciar los sockets y canales
        //no hay id de usuario osea no hay login aún bro
        this.userChannel = null;
      }
    });
  }

  userCallbacks() {
    this.userChannel.onError(() => {
      this.subscription.unsubscribe();
      this.userChannel.leave();
      
    });
    this.userChannel.onClose(() => {
      //Se cierra el canal
      console.log("closing connection!");
      this.subscription.unsubscribe();
      this.dashboardStore.leave();
    });
    window.addEventListener("beforeunload", () => {
      this.versusStore.searching(false);
      this.dashboardChannel.push("disconnect", null);
      this.userChannel.push("disconnect", null);
    });
    this.userChannel.on("user_update", payload => {
      console.log(payload);
      this.sessionService.update_state(payload);
    })
    this.userChannel.on("finish", payload => {
      this.lobbyService.createFalseLobby("fives");
      this.lobbyService.test(payload);
    })
    this.userChannel.on("search_status", payload => {
      //console.log(payload);
      this.versusStore.searching(true, payload);
    });
    this.userChannel.on("stop_search", payload => {
      //console.log("stop search bro!")
      this.snackbar.open("The search has been stopped")
      this.versusStore.stopSearch();
    });

    this.userChannel.on("friend_request", payload => {
      
      let friend: Friend = {
        friend_username: payload.friend_username,
        friend_user_id: payload.friend_user_id,
        status: payload.status,
        profile: payload.profile,
        connected: false
      }
      this.sessionService.addFriend(friend);
      this.options.toastComponent = FriendRequestComponent;
      this.options.tapToDismiss = false;
      let tryhard = JSON.stringify(payload);
      this.toastr.info(tryhard, 'aaaaaa', this.options);
    });
    this.userChannel.on("steamid_update", payload => {
      this.sessionService.updateSteamid(payload.steamid);
    })
    this.userChannel.on("friend_request_accepted", payload => {
      let friend: Friend = {
        friend_username: payload.friend_username,
        friend_user_id: payload.friend_user_id,
        status: payload.status,
        profile: payload.profile,
        connected: false
      }
      let local_id = this.sessionQuery.getSnapshot().id;
      if(local_id != friend.friend_user_id){
        this.sessionService.addFriend(friend);
      }
      
    });
    this.userChannel.on("team_region_changed", payload => {
      this.versusStore.setRegion(payload.region);
    });
    this.userChannel.on("team_bet_changed", payload => {
      this.versusStore.setBet(`${payload.bet_per_user}`)
    })
    this.userChannel.on("wanna_play", payload => {
      this.versusStore.update_wanna_play(payload);
    });
    this.userChannel.on("ask_nothing", payload => {
      this.versusStore.close_wanna_play();
      this.versusStore.stopSearch();
    })
    this.userChannel.on("ask_cancelled", payload => {
      this.snackbar.open("Alguíen no aceptó, reiniciando la búsqueda...")
      let bet = this.versusQuery.getSnapshot().bet;
      let region = this.versusQuery.getSnapshot().region;
      this.versusStore.stopSearch();
      this.versusStore.close_wanna_play();
      let params = {
        "bet":  bet,
        "mode": "fives",
        "region": region
      }
      this.search(params);
    });
    this.userChannel.on("ask_done", payload => {
      console.log("ask done do something!");
    });
    this.userChannel.on("update_acepted", payload => {
      console.log(payload);
      this.versusStore.update_wanna_play(payload);
    });
    this.userChannel.on("send_question", payload => {
      let selected = new Subject();
      this.dialog.open(QuestionaryComponent, {
        data: {question: payload.question, result: selected},
        escapeToClose: false,
        clickOutsideToClose: false,
        buttonsStacked: false,
        id: "23"
      });

      selected.subscribe(result => {
        this.questionAnswer(result, payload.question_id);
        selected.unsubscribe();
      });
    });
    this.userChannel.on("all_acepted", payload => {
      if(this.wannaRef != null){
        this.wannaRef.close();
        this.router.navigate(['versus'])
      }
      this.lobbyService.matchFinded(payload);
      this.versusStore.stopSearch();
      //this.stopSearch();
    });
    this.userChannel.on("join_team", payload => {
      this.versusStore.setRegion(payload.region);
      this.versusStore.setBet(`${payload.bet_per_user}`)
      this.getTeamUsers(payload);
    });
    this.userChannel.on("accepted_team_invitation", payload => {
      
    })
    this.userChannel.on("leader_team_join", payload => {
      /*this.teamChannel = this.socketService.connectTo(`team:${payload["code"]}`)
      this.teamChannel.join().receive("ok", response => {
        
      })*/
    });
    this.userChannel.on("kicked_team", payload => {
      
      this.versusStore.exit_team();
    })
    this.userChannel.on("player_leave_team", payload => {
      this.getTeamUsers(payload);
    });
    this.userChannel.on("team_invitation", payload => {
      
      this.options.toastComponent = InviteComponent;
      this.options.timeOut = 0;
      this.options.tapToDismiss = false;
      this.options.extendedTimeOut = 0;
      this.options.toastClass = "invitecomponent";
      let lastToastr = this.toastr.show("Accept lobby invitation", "Friend invite!", this.options);
      lastToastr.onAction.subscribe(result => {
        if(result.action == "accept"){
          const params = {
            leader_id: result.data.leader_id
          };
          //Obtener imagen y nombre del usuario leader_id
          this.userChannel.push("accept_friend_invitation", params);
        }
      });
      this.inviteTostrSerivce.createToastr(lastToastr.toastId, payload["leaderId"], "asdf", payload["friendName"]);
    });
    this.userChannel.on("finded", payload => {
      
      this.lobbyService.matchFinded(payload);
    });
    this.userChannel.join().receive("ok", response => {
      console.log("User channel connected!");
    });
  }

  dashboardCallbacks() {
    /** OnSomething hooks */
    this.dashboardChannel.onError(() => {
      this.subscription.unsubscribe();
      this.dashboardChannel.leave();
    });
    this.dashboardChannel.onClose(() => {
      //window.location.reload();
    });

    /** EVENTS */
    this.dashboardChannel.on("presence_state", res => {
      this.presences = Presence.syncState(this.presences, res);
      this.dashboardStore.storePresences(this.presences);
      if(this.channelLoaded == false){
        this.channelLoaded = true;
        this.userCallbacks();
        this.dashboardStore.join({connected: true, usersConnected: this.presences});
      }
    })
    this.dashboardChannel.on("presence_diff", res => {
      this.presences = Presence.syncDiff(this.presences, res);
      this.dashboardStore.storePresences(this.presences);
    });

    /** Connection to socket */
    this.dashboardChannel.join().receive("ok", response => {
      console.log(`connected to dashboard :D`);
      console.log(response);
    });
  }

  getTeamUsers(payload){
    console.log(payload);
    let tries = 0;
    this.versusStore.loadingTeam(true);
    let interval = setInterval(() => {
      this.versusStore.loadingTeam(true);
      if(payload.team.length == 0){
        let id = this.sessionQuery.getSnapshot().id;
        this.leave_team(id);
        this.snackbar.open("Todos tus compañeros se han salido del equipo");
        this.versusStore.loadingTeam(false);
        clearInterval(interval);
      } else {
        let player = this.getPlayerFromPresences(payload.leader); 
        let val = 0;
        let size = payload.team.length + 1;
        let tmp = [];
        if(player != null) val++;
        for(let i = 0; i < payload.team.length; i++) {
          let temp_player = this.getPlayerFromPresences(payload.team[i]);
          if(temp_player != null) {
            tmp.push(temp_player);
            val++;
          }
        }
        if(val == size){
          let team: Team = {
            team: tmp,
            leader: player,
            code: payload.code
          }
          this.snackbar.open("Equipo creado de forma correcta");
          this.versusStore.create_team(team);
          this.versusStore.loadingTeam(false);
          clearInterval(interval);
        } else {
          tries++;
          if(tries == 10){
            this.snackbar.open("No se ha podido actualizar el equipo, se te ha echado del equipo");
            let id = this.sessionQuery.getSnapshot().id;
            this.leave_team(id);
            this.versusStore.loadingTeam(false);
            clearInterval(interval);
          } else {
            this.snackbar.open(`Actualizando equipo intento: ${tries}`);
          }
        }
      }
    }, 1000);
  }

  listUsers() {
    Presence.list(this.presences, (id, {metas: metas}) => {
      
    });
  }

  getPlayerFromPresences(player_id): Player {
    let tmp = null;
    Presence.list(this.presences, (id, {metas: metas}) => {
      
      if(id == player_id) {
        let player: Player = {
          id: id,
          username: metas[0].user,
          picture: metas[0].picture,
        }
        if(player.picture == null){
          player.picture = "/assets/img/dashboard/default_avatar.png";
        }
        tmp = player;
      }
    });
    
    return tmp;
  }

  isUserConnected(username): boolean {
    let tmp = [];
    Presence.list(this.presences, (id, {metas: metas}) => {
      tmp.push(metas[0].user);
    });
    for(let i = 0; i < tmp.length; i++){
      if(tmp[i] == username) return true;
    }
    return false;
  }

  leave_team(leaver_id) {
    const params = {
      leaver_id: leaver_id
    }

    this.userChannel.push("leave_team", params);
  }

  change_team_region(leader_id, region) {
    const params = {
      leader_id: leader_id,
      region: region
    }

    this.userChannel.push("change_region", params);
  }

  change_team_bet(leader_id, bet) {
    const params = {
      leader_id: leader_id,
      bet: bet
    }

    this.userChannel.push("change_bet", params);
  }

  getFriendsConnected(friends) {
    let temp = [];
    Presence.list(this.presences, (id, {metas: metas}) => {
      for(let i = 0; i < friends.length; i++) {
        if(friends[i].friend_username == metas[0].user){
          temp.push(friends[i]);
        }
      }
    });
    return temp;
  }

  declineInvitation(leader_id, user_id){
    const params = {
      leader_id: leader_id,
      user_id: user_id
    }

    this.userChannel.push("decline_friend_invitation", params);
  }

  inviteFriend(friend_id, region, bet){
    const params = {
      friend_id: friend_id,
      region: region,
      bet: bet
    }
    this.userChannel.push("invite_friend", params);
  }

  questionAnswer(result, id){
    const params = {
      answer: result,
      id: id
    }
    this.userChannel.push("recieve_answer", params);
  }

  changeFile(event) {
    this.fileChangeEvent = of(event);
  }

  aceptMatch() {
    this.userChannel.push("accept_match", null);
  }

  search(params: any) {
    //this.versusStore.searching(true, params);
    this.userChannel.push("search_match", params)
  }

  closeAllChannels(){
    if(this.userChannel != null){
      this.userChannel.leave();
    }
    if(this.dashboardChannel != null){
      this.dashboardChannel.leave();
    }
    
  }

  stopSearch(){
    this.userChannel.push("leave", null);
    this.versusStore.stopSearch();
  }

}
