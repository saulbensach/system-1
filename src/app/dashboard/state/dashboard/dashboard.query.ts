import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { DashboardStore } from './dashboard.store';
import { DashboardState } from './dashboard.model';

@Injectable({ providedIn: 'root' })
export class DashboardQuery extends Query<DashboardState> {
  isConnected$ = this.select(state => state.connected);
  presences$ = this.select(state => state.usersConnected);

  constructor(protected store: DashboardStore) {
    super(store);
  }

}
