import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { DashboardState } from './dashboard.model';
import * as storage from '../../../storage';

export function createInitialState(): DashboardState {
  return {
    connected: false,
    usersConnected: null,
    ...storage.getDashboard()
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'dashboard' })
export class DashboardStore extends Store<DashboardState> {

  constructor() {
    super(createInitialState());
  }

  storePresences(presence) {
    let dashboard = storage.getDashboard();
    dashboard.usersConnected = presence;
    this.update(dashboard);
    storage.saveDashboard(dashboard);
  }

  join(dashboard: DashboardState) {
    this.update(dashboard);
    storage.saveDashboard(dashboard);
  }

  leave(){
    storage.clearDashboard();
    this.update(createInitialState());
  }

}

