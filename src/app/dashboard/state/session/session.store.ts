import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import * as storage from '../../../storage';
import { SessionState } from './session.model';

export function createInitialState(): SessionState {
  return {
    token: null,
    name: null,
    username: null,
    surname: null,
    steamid: null,
    picture: "/assets/img/dashboard/default_avatar.png",
    banner: null,
    notifications: null,
    friends: [],
    id: null,
    balance: null,
    ...storage.getSession()
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'session' })
export class SessionStore extends Store<SessionState> {

  constructor() {
    super(createInitialState());
  }

  addFriend(friend){
    let session = storage.getSession();
    session.friends.push(friend);
    storage.saveSession(session);
    this.update(session);
  }

  removeFriend(id){
    let session = storage.getSession();
    let aux = [];
    for(let friend of session.friends){
      if(friend.friend_user_id === id){continue;}
      else{
        aux.push(friend);
      }
    }
    session.friends = aux;
    storage.saveSession(session);
    this.update(session);
  }

  changeFriendStatus(id, status){
    let session = storage.getSession();
    for (let i = 0; i < session.friends.length; i++){
      if(session.friends[i].friend_user_id === id){
        session.friends[i].status = status;
      }
    }
    this.update(session);
    storage.saveSession(session);
  }

  updateSteamid(steamid){
    let session = storage.getSession();
    session.steamid = steamid;
    storage.saveSession(session);
    this.update(session);
  }

  state_update(session: SessionState) {
    this.update(session);
    storage.saveSession(session);
  }

  login(session: SessionState) {
    this.update(session);
    storage.saveSession(session);
  }

  changeProfileUrl(url) {
    let session = storage.getSession();
    session.picture = url;
    storage.saveSession(session);
    this.update(session);
  }

  changeBannerUrl(url) {
    let session = storage.getSession();
    session.banner = url;
    storage.saveSession(session);
    this.update(session);
  }

  logout() {
    storage.clearSession();
    this.update(createInitialState());
  }

}

