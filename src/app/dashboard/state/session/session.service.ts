import { Injectable } from '@angular/core';
import { SessionStore } from './session.store';

@Injectable({ providedIn: 'root' })
export class SessionService {

  userChannel: any;

  constructor(
    private sessionStore: SessionStore
  ) {
  }

  addFriend(friend){
    this.sessionStore.addFriend(friend);
  }

  removeFriend(id){
    this.sessionStore.removeFriend(id);
  }

  changeFriendStatus(id, status){
    this.sessionStore.changeFriendStatus(id, status);
  }

  updateSteamid(steamid){
    this.sessionStore.updateSteamid(steamid);
  }
  
  login(session) {
    this.sessionStore.logout();
    this.sessionStore.login(session);
  }

  changeUrlProfile(url) {
    this.sessionStore.changeProfileUrl(url);
  }

  update_state(session) {
    this.sessionStore.state_update(session);
  }

  changeUrlBanner(url) {
    this.sessionStore.changeBannerUrl(url);
  }

  register(session) {
    this.sessionStore.login(session);
  }

  logout() {
    this.sessionStore.logout();
  }

}
