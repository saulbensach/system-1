import {Notifications} from '../../../models/notifications.model';
import { Friend } from 'src/app/models/friend.model';

export interface SessionState {
  token: string;
  name: string;
  username: string;
  surname: string;
  picture: string;
  banner: string;
  friends: Friend[];
  notifications: Notifications;
  id: number;
  steamid: string;
  balance: string;
}