import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { SessionStore } from './session.store';
import { toBoolean } from '@angular-mdc/web';
import { SessionState } from './session.model';

@Injectable({ providedIn: 'root' })
export class SessionQuery extends Query<SessionState> {
  isLoggedIn$ = this.select(state => toBoolean(state.token));
  steamid$ = this.select(state => toBoolean(state.steamid));
  name$ = this.select(state => state.name);
  username$ = this.select(state => state.username);
  surname$ = this.select(state => state.surname);
  token$ = this.select(state => state.token);
  balance$ = this.select(state => state.balance);
  id$ = this.select(state => state.id);
  picture$ = this.select(state => state.picture);
  banner$ = this.select(state => state.banner);
  friends$ = this.select(state => state.friends);
  // fixear poner el plural en vez de singular que esto es un array
  notification$ = this.select(state => state.notifications);

  constructor(protected store: SessionStore) {
    super(store);
  }

  isLoggedIn(){
    return toBoolean(this.getSnapshot().token);
  }

}
