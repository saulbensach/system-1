import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsComponent } from './teams.component';
import { teamsRouting } from './teams.routing';
import { MdcTabBarModule, MdcIconModule, MdcIconButtonModule, MdcImageListModule, MdcTypographyModule, MdcTextFieldModule, MdcButtonModule, 
  MdcElevationModule, MdcDialogModule} from '@angular-mdc/web';

@NgModule({
  imports: [
    CommonModule,
    teamsRouting,
    MdcTabBarModule,
    MdcIconModule,
    MdcIconButtonModule,
    MdcImageListModule,
    MdcTypographyModule,
    MdcTextFieldModule,
    MdcButtonModule,
    MdcElevationModule,
    MdcDialogModule
  ],
  declarations: [TeamsComponent]
})
export class TeamsModule { }
