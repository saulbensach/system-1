import { Routes, RouterModule } from "@angular/router";
import { TeamsComponent } from "./teams.component";
import { ModuleWithProviders } from "@angular/compiler/src/core";

export const teamsRoutes: Routes = [
    {
        path: '',
        component: TeamsComponent
    }
];

export const teamsRouting: ModuleWithProviders = RouterModule.forChild(teamsRoutes);