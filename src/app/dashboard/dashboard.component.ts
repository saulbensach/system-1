import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { SocketQuery, SocketService } from "./state/socket";
import { DashboardService } from "./state/dashboard";

@Component({
    selector: 'app-dashboard',
    template: '<router-outlet></router-outlet>'
})
export class DashboardComponent implements OnInit, OnDestroy {
    subscription: Subscription;
    constructor(
        private socketQuery: SocketQuery,
        private dashboardService: DashboardService,
        private socketService: SocketService
    ){
        this.subscription = this.socketQuery.isConnected$.subscribe(islogged => {
            if(islogged){
                //this.dashboardService.join();
                //this.aux = true;
            } else {
                //Tal vez desconectar al usuario del canal etc
                /*if(this.aux == true){
                    //this.dashboardService.leave();
                    //this.aux == true;
                }*/
            }
        });        
    }

    ngOnInit() {}
    
    ngOnDestroy() {
        this.socketService.disconnect();
        this.subscription.unsubscribe();
    }

}

