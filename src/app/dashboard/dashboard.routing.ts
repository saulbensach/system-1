import { DashboardComponent } from "./dashboard.component";
import { ModuleWithProviders } from "@angular/compiler/src/core";
import { RouterModule, Routes } from "@angular/router";
import { ConfirmEmailComponent } from "./confirm-email/confirm-email.component";

export const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        children: [
            {
                path: '', redirectTo: 'home', pathMatch: 'full'
            },
            {
                path: 'home',
                loadChildren: './home/home.module#HomeModule'
            },
            {
                path: 'versus',
                loadChildren: './versus/versus.module#VersusModule'
            },
            {
                path: 'profile/:name',
                loadChildren: './profile/profile.module#ProfileModule'
            },
            {
                path: 'wallet',
                loadChildren: './wallet/wallet.module#WalletModule'
            },
            {
                path: 'challenges',
                loadChildren: './challenges/challenges.module#ChallengesModule'
            },
            {
                path: 'settings',
                loadChildren: './settings/settings.module#SettingsModule'
            },
            {
                path: 'chat-room',
                loadChildren: './chat-room/chat-room.module#ChatRoomModule'
            },
            {
                path: 'help',
                loadChildren: './help/help.module#HelpModule'
            },
            {
                path: 'team',
                loadChildren: './teams/teams.module#TeamsModule'
            }
        ]
    },
    {
        path: 'confirmation/:token',
        component: ConfirmEmailComponent
    },
    {
        path: 'confirmation',
        redirectTo: 'home'
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);