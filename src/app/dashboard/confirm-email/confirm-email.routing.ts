import { Routes, RouterModule } from "@angular/router";
import { ConfirmEmailComponent } from "./confirm-email.component";
import { ModuleWithProviders } from "@angular/compiler/src/core";

export const confirmEmailRoutes: Routes = [
    {
        path: '',
        component: ConfirmEmailComponent,
        data: {
            pageTitle: 'confirmation'
        }
    }
];

export const confirmEmailRouting: ModuleWithProviders = RouterModule.forChild(confirmEmailRoutes);