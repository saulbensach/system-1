import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    if (this.route.snapshot.params['token'] != null && this.route.snapshot.params['token'] != "") {
      this.api.sendTokenEmail(this.route.snapshot.params['token']).subscribe();
    }
  }

  goHome(){
    this.router.navigate(['home'])
  }

}
