import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './dashboard.routing';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './state/dashboard';
import { DialogsModule } from '../shared/dialogs/dialogs.module';
import { MdcSnackbarModule, MdcCardModule, MdcListModule, MdcTextFieldModule, MdcButtonModule, MdcIconModule, MdcIconButtonModule, MdcElevationModule, 
  MdcFormFieldModule, MdcCheckboxModule } from '@angular-mdc/web';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';

@NgModule({
  imports: [
    CommonModule,
    routing,
    DialogsModule,
    MdcSnackbarModule,
    MdcCardModule,
    MdcListModule,
    MdcTextFieldModule,
    MdcButtonModule,
    ReactiveFormsModule,
    MdcIconModule,
    MdcIconButtonModule,
    MdcElevationModule,
    MdcFormFieldModule,
    FormsModule,
    MdcCheckboxModule
  ],
  declarations: [DashboardComponent, ConfirmEmailComponent],
  providers: []
})
export class DashboardModule { }
