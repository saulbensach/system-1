import { Component, OnInit, OnDestroy, isDevMode, ViewChild, AfterViewInit, AfterViewChecked, AfterContentInit, AfterContentChecked } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { VersusStore, VersusQuery } from './state';
import { DashboardService } from '../state/dashboard';
import { MdcListItem, MdcDialog, MdcDialogRef, MdcSelect, MdcList } from '@angular-mdc/web';
import { DialogsService, DialogsQuery } from 'src/app/shared/dialogs/state';
import { Subscription } from 'rxjs';
import { InviteFriendsComponent } from 'src/app/shared/dialogs/invite-friends/invite-friends.component';
import { SessionQuery } from '../state/session';
import { Player } from './state/player.model';
import { environment } from 'src/environments/environment';
import { environmentProd } from 'src/environments/environment.prod';
import { AskherComponent } from 'src/app/shared/dialogs/askher/askher.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-versus',
  templateUrl: './versus.component.html',
  styleUrls: ['./versus.component.scss'],
  providers: []
})

export class VersusComponent implements OnInit, OnDestroy, AfterContentChecked {

  @ViewChild('region') regions: MdcSelect;
  @ViewChild('betList') betList: MdcList;

  url: string;
  wannaRef: MdcDialogRef<AskherComponent>;
  loading: Observable<boolean>;
  selectedBet: string;
  team: Observable<any>;
  in_team: boolean;
  has_steam_id: boolean;
  is_local_leader: boolean;
  friend_team1: Player;
  friend_team1_in: boolean;
  friend_team1_index: number;
  friend_team2: Player;
  friend_team2_in: boolean;
  friend_team2_index: number;
  friend_team3: Player;
  friend_team3_in: boolean;
  friend_team3_index: number;
  friend_team4: Player;
  friend_team4_in: boolean;
  friend_team4_index: number;
  token$: Observable<any>;
  allSelected: Observable<boolean>;
  gameisSelected: string;
  defaultRegion: Observable<string>;
  params: any;
  listSub: Subscription;
  profilePic$: Observable<any>;
  modeSelected: string;
  betSelected: string;
  modes: string[];
  betStore: Observable<string>;
  region: Observable<string>;
  index: number;
  subscription: Subscription;
  bets: string[] = [
    "1",
    "2",
    "5",
    "10",
    "15",
    "20"
  ];
  enabled: boolean;
  loaded: boolean;
  gamesMenu = [
    { name: "Counter-Strike: Global Offensive" },
    { name: "Player Unknown Battle Ground" },
    { name: "Rocket League" },
    { name: "Team Fortress 2" }
  ];

  constructor(
    private dashboardService: DashboardService,
    private dialogService: DialogsService,
    private dialogQuery: DialogsQuery,
    private sessionQuery: SessionQuery,
    private versusQuery: VersusQuery,
    private versusStore: VersusStore,
    private dialog: MdcDialog
  ) { 
    this.loaded = false;
    if(isDevMode()){
      this.url = environment.steam_auth
    } else {
      this.url = environmentProd.steam_auth;
    }
    // TODO Arreglar esta vaina
    this.loading = this.versusQuery.loading$;
    this.betStore = this.versusQuery.bet$;
    this.has_steam_id = false;
    this.resetTeams();
    this.token$ = this.sessionQuery.token$;
    this.is_local_leader = false;
    this.friend_team1_in = false;
    this.in_team = false;
    this.subscription = this.dialogQuery.opened$.subscribe(value => {
      if(value){
        this.dialogService.getInviteDialog().beforeClosed().subscribe(() => {
          this.dialogService.closeDialog();
        });
      }
    });

    this.versusQuery.askher$.subscribe(state => {
      if(state){
        let clicked = new Subject();
        this.wannaRef = this.dialog.open(AskherComponent, {
          data: {emitter: clicked},
          escapeToClose: false,
          clickOutsideToClose: false,
          buttonsStacked: false,
          id: "ask-dialog"
        });
        clicked.subscribe(() => {
          this.versusStore.acceptAsk();
          this.dashboardService.aceptMatch();
          clicked.unsubscribe();
        })
      } else {
        if(this.wannaRef != null){
          this.wannaRef.close();
        }
      }
    })

    this.sessionQuery.steamid$.subscribe(steamid => {
      if(steamid == false){
        this.has_steam_id = false;
      } else {
        this.has_steam_id = true;
      }
    })
    this.index = 0;
    //this.modes = ["5 vs 5", "ffa"]
    this.modes = ["1 vs 1"]
  }

  ngAfterContentChecked() {
    this.loaded = true;
  }

  selectChange(event: { option: any, source: MdcList }){
  }

  ngOnInit() {
    this.region = this.versusQuery.region$;
    let interval = setInterval(() => {
      if(this.loaded == true){
        let region = this.versusQuery.getSnapshot().region;
        this.versusQuery.bet$.subscribe(bet => {
          if(bet != "-1" && bet != "0"){
            this.betSelected = bet;
            if(this.betList != undefined) {
              this.betList.setSelectedIndex(this.bets.indexOf(bet));
            }
          }
        });
        if (this.regions != undefined) {
          if(region === "euw"){
            this.regions.setSelectedIndex(0);
          } else if(region === "latam") {
            this.regions.setSelectedIndex(1);
          } else {
            this.regions.reset();
          }
        }
        clearInterval(interval);
      }
    }, 200);
    this.defaultRegion = this.versusQuery.region$;
    this.versusQuery.team$.subscribe(team => {
      if(team != null){
        if(team.team != null && team.leader != null){
          this.in_team = true;
          let team_list = team.team;
          let id = this.sessionQuery.getSnapshot().id;
          if(id == team.leader.id){
            this.is_local_leader = true;
          } else {
            this.is_local_leader = false;
          }
          //hardcode of all mothers
          let local_id =this.sessionQuery.getSnapshot().id;
          switch(team_list.length){
            case 0 : {
              this.resetTeams();
              break;
            }
            case 1: {
              this.resetTeams();
              this.friend_team1_in = true;
              //this.friend_team1 = team_list[0];
              if(this.is_local_leader) {
                this.friend_team1 = team_list[0];
                this.friend_team1_index = 0;
              } else {
                this.friend_team1 = team.leader;
              }
              break;
            }
            case 2: {
              this.resetTeams();
              let tmp = [];
              //Quitamos el pavo del equipo con el mismo id que el local
              for(let i = 0; i < team_list.length; i++){
                if(local_id == team_list[i].id)continue;
                tmp.push(team_list[i]);
              }
              //El array se ordena de mayor a menor así que el team_list[1] es el último en entrar
              this.friend_team1_in = true;
              this.friend_team2_in = true;
              if(this.is_local_leader) {
                this.friend_team1_index = 1;
                this.friend_team2_index = 0;
                this.friend_team1 = tmp[1];
                this.friend_team2 = tmp[0];
              } else {
               this.friend_team1 = team.leader;
               this.friend_team2 = tmp[0];
               this.friend_team2_index = 0;
              }
              break;
            }
            case 3: {
              this.resetTeams();
              let tmp = [];
              //Quitamos el pavo del equipo con el mismo id que el local
              for(let i = 0; i < team_list.length; i++){
                if(local_id == team_list[i].id)continue;
                tmp.push(team_list[i]);
              }
              this.friend_team1_in = true;
              this.friend_team2_in = true;
              this.friend_team3_in = true;
              if(this.is_local_leader){
                this.friend_team1_index = 2;
                this.friend_team2_index = 1;
                this.friend_team3_index = 0;
                this.friend_team1 = team_list[2];
                this.friend_team2 = team_list[1];
                this.friend_team3 = team_list[0];
              } else {
                this.friend_team1_index = -1;
                this.friend_team2_index = 0;
                this.friend_team3_index = 1;
                this.friend_team1 = team.leader;
                this.friend_team2 = tmp[0];
                this.friend_team3 = tmp[1];
              }
              break;
            }
            case 4: {
              this.resetTeams();
              let tmp = [];
              //Quitamos el pavo del equipo con el mismo id que el local
              for(let i = 0; i < team_list.length; i++){
                if(local_id == team_list[i].id)continue;
                tmp.push(team_list[i]);
              }
              this.friend_team1_in = true;
              this.friend_team2_in = true;
              this.friend_team3_in = true;
              this.friend_team4_in = true;
              if(this.is_local_leader){
                this.friend_team1_index = 3;
                this.friend_team2_index = 2;
                this.friend_team3_index = 1;
                this.friend_team4_index = 0;
                this.friend_team1 = team_list[3];
                this.friend_team2 = team_list[2];
                this.friend_team3 = team_list[1];
                this.friend_team4 = team_list[0];
              } else {
                this.friend_team1_index = -1;
                this.friend_team2_index = 0;
                this.friend_team3_index = 1;
                this.friend_team4_index = 2;
                this.friend_team1 = team.leader;
                this.friend_team2 = tmp[0];
                this.friend_team3 = tmp[1];
                this.friend_team4 = tmp[2];
              }
              break;
            }
          }
        } else {
          this.in_team = false;
          this.resetTeams();
          this.is_local_leader = false;
        }
      } else {
        this.in_team = false;
        this.resetTeams();
        this.is_local_leader = false;
      }
    });
    this.profilePic$ = this.sessionQuery.picture$;
    this.betSelected = "-1";
    this.modeSelected = this.modes[this.index];
    this.enabled = false;
    this.gameisSelected = "";
  }

  resetTeams(){
    this.friend_team1 = null;
    this.friend_team2 = null;
    this.friend_team3 = null;
    this.friend_team4 = null;
    this.friend_team1_in = false;
    this.friend_team2_in = false;
    this.friend_team3_in = false;
    this.friend_team4_in = false;
    this.friend_team1_index = -1;
    this.friend_team2_index = -1;
    this.friend_team3_index = -1;
    this.friend_team4_index = -1;
  }

  onSelectionChange(event: { index: any, value: any }){
    if(event.value === "euw" || event.value === "latam"){
      let current = this.versusQuery.getSnapshot().region;
      this.versusStore.setRegion(event.value);
      if(this.versusQuery.getSnapshot().team != null){
        let leader_id = this.versusQuery.getSnapshot().team.leader.id;
        if(current != event.value){
          this.dashboardService.change_team_region(leader_id, event.value)
        }
      }
    }
  }

  leaveTeam(){
    this.friend_team1_in = false;
    let leader_id = this.versusQuery.getSnapshot().team.leader.id;
    let leaver_id = this.sessionQuery.getSnapshot().id;
    this.versusStore.exit_team();
    this.dashboardService.leave_team(leaver_id);
  }

  dropTeammate(arrayPosition){
    this.friend_team1_in = false;
    
    let leaver_id = this.versusQuery.getSnapshot().team.team[arrayPosition].id;

    this.dashboardService.leave_team(leaver_id);
  }

  gameSelected(game) {
    
    switch(game) {
      case 'csgo':
        this.gameisSelected = "csgo";
        break;
      case 'pubg':
        this.gameisSelected = "pubg";
        break;
      case 'rocketleague':
        this.gameisSelected = "rocketleague";
        break;
      case 'teamfortress':
        this.gameisSelected = "teamfortress";
        break;
    }
  }

  test(index: number) {
    this.betSelected = this.bets[index];
    this.versusStore.setBet(this.bets[index]);
    if(this.versusQuery.getSnapshot().team != null){
      let leader_id = this.versusQuery.getSnapshot().team.leader.id;
      this.dashboardService.change_team_bet(leader_id, this.betSelected);
    }
  }

  getNewIndex(dir) {

    switch(dir) {
      case 'next': 
        this.index = (this.index + 1) % this.modes.length;
      break;
      case 'prev': 
        if(this.index == 0) {
          this.index = this.modes.length - 1;
      
          this.index = (this.index - 1) % this.modes.length;
        }
      break;
    }

    this.modeSelected = this.modes[this.index];
    if (this.modeSelected == "5 vs 5") {
      this.enabled = true;
    } else {
      this.enabled = false;
    }
  }

  inviteFriend(){
    let friends = this.sessionQuery.getSnapshot().friends;
    let friends_connected = this.dashboardService.getFriendsConnected(friends);
    let selected = new Subject();
    this.dialog.open(InviteFriendsComponent, {
      data: {payload: friends_connected, friend_id: selected},
      escapeToClose: true,
      clickOutsideToClose: true,
      buttonsStacked: false,
      id: "invite-friends"
    });

    selected.subscribe(friend_id => {
      let region = this.versusQuery.getSnapshot().region;
      let bet = this.versusQuery.getSnapshot().bet;
      this.dashboardService.inviteFriend(friend_id, region, bet);
    });
  }


  searchMatch() {
    
    let mode = this.modeSelected;
    if(this.modeSelected == "5 vs 5") {
      mode = "fives";
    } else if(this.modeSelected == "1 vs 1") {
      mode = "solo";
    }
    if(this.modeSelected != "-1" && this.selectedBet != "-1"){
      
      let params = {
        "bet": this.betSelected,
        "mode": mode,
        "region": this.versusQuery.getSnapshot().region
      }
      this.dashboardService.search(params);
    }else{
      //porq else bro
    }
  }

  search(params: any) {
    this.params = params;
    
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  stopSearch(){
    //this.versusService.stopSearch();
    this.index = 0;
    this.betSelected = "-1";
    this.modeSelected = this.modes[this.index];
    this.dashboardService.stopSearch();
   //this.versus.leaveSearch(false);
  }

  onMenuSelect(event: { index: number, item: MdcListItem }) {
    
  }

}
