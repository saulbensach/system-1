import { Routes, RouterModule } from "@angular/router";
import { VersusComponent } from "./versus.component";
import { ModuleWithProviders } from "@angular/compiler/src/core";

export const versusRoutes: Routes = [
    {
        path: '',
        component: VersusComponent,
        data: {
            pageTitle: 'Versus'
        }
    }
];

export const versusRouting: ModuleWithProviders = RouterModule.forChild(versusRoutes);