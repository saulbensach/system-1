import { Message } from "./messages.model";
import { FiveStats } from "src/app/models/fives-stats.model";
import { FivesUsers } from "../fives/fives.model";

export interface LobbyState {
    match_finded: boolean;
    lobby_token: string;
    messages: Message[];
    presences: any;
    gamemode: string;
    team1: FivesUsers[];
    team2: FivesUsers[];
    map_selected: string;
    ip: string;
    result: FiveStats;
    total_bet: string;
    team: number;
}