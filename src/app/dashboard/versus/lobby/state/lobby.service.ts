import { Injectable } from '@angular/core';
import { LobbyStore } from './lobby.store';
import { SocketService } from 'src/app/dashboard/state/socket';
import { LobbyQuery } from './lobby.query';
import { Subscription } from 'rxjs';
import { FivesUsers } from '../fives/fives.model';
import { ApiService } from 'src/app/shared/services/api.service';
import { Router } from '@angular/router';
import { MdcSnackbar } from '@angular-mdc/web';

@Injectable({ providedIn: 'root' })
export class LobbyService {

  lobbyChannel: any;
  teamChannel: any;
  team: any;
  subscription: Subscription;

  constructor(
    private lobbyStore: LobbyStore,
    private lobbyQuery: LobbyQuery,
    private socket: SocketService,
    private snackbar: MdcSnackbar,
    private router: Router,
    private api: ApiService
  ) {
    this.subscription = this.lobbyQuery.matchFinded$.subscribe(finded => {
      if(finded) {
        this.lobbyQuery.lobby_token$.subscribe(token => {
          if(token != null) {
            this.lobbyQuery.team$.subscribe(team => {
              this.team = team;
              //this.teamChannel = this.socket.connectTo(`team:${token}${team}`)  
            });
            this.lobbyChannel = this.socket.connectTo(`lobby:${token}`);
            this.lobbyChannel.onClose(() => {
              
              this.subscription.unsubscribe();
              this.lobbyStore.deleteLobby();
            });
            this.lobbyChannel.on("new_msg", payload => {
              this.lobbyStore.appendAllChatMessage(payload);
            });
            this.lobbyChannel.on("map_selected", payload => {
              this.lobbyStore.set_map(payload);
            });
            this.lobbyChannel.on("match_canceled", payload => {
              this.snackbar.open("The match has been cancelled by the server!");
              this.subscription.unsubscribe();
              this.lobbyStore.deleteLobby();
            });
            this.lobbyChannel.on("lobby_users", payload => {
              let team1 :FivesUsers[] = [];
              let team2 :FivesUsers[] = [];
              for(let i = 0; i < payload.team1.length; i++){
                const player: FivesUsers = {
                  username: payload.team1[i].name,
                  profile: payload.team1[i].picture
                }
                team1.push(player);
              }
              for(let i = 0; i < payload.team2.length; i++){
                const player: FivesUsers = {
                  username: payload.team2[i].name,
                  profile: payload.team2[i].picture
                }
                team2.push(player);
              }
              this.lobbyStore.set_teams(team1, team2);
            });
            this.lobbyChannel.on("finded", payload => {
              this.lobbyStore.set_ip(payload);
            });
            this.lobbyChannel.on("finish", payload => {
              this.lobbyStore.matchEnded(payload);
            });
            this.lobbyChannel.join().receive("ok", data => {
              
            });
          }
        })
      } else {
        if(this.lobbyChannel != null) {
          this.subscription.unsubscribe();
          this.lobbyStore.deleteLobby();
        }

        this.lobbyChannel = null;
      }
    });
  }

  test(payload){
    this.lobbyStore.matchEnded(payload);
  }

  test2(){
    this.lobbyStore.soloTestEnd();
  }

  sendTeamChat(message: string) {
    //this.teamChannel.push("message:new", {data: message});
  }

  sendMapSelected(message: string) {
    this.lobbyChannel.push("map_selection", {data: message});
  }

  sendAllChat(message: string) {
    this.lobbyChannel.push("message:new", {data: message});
  }

  matchFinded(payload) {
    this.lobbyStore.updateLobby({
      match_finded: true,
      lobby_token: payload.token, 
      presences: {},
      messages: [],
      gamemode: "fives",
      map_selected: '',
      team1: null,
      team2: null,
      result: null,
      total_bet: payload.bet,
      ip: null,
      team: payload.team
    });
  }

  showMatch(id){
    this.api.getHistory(id).subscribe(result => {
      //console.log(result);
      this.router.navigate(['versus']);
      this.lobbyStore.showMatch(result);
    })
  }

  createFalseLobby(gamemode){
    this.lobbyStore.updateLobby({
      match_finded: true,
      lobby_token: "abcd", 
      presences: {},
      messages: [],
      gamemode: gamemode,
      team1: null,
      team2: null,
      map_selected: "",
      result: null,
      total_bet: "20",
      ip: 'creating_server',
      team: 1
    });
  }

  leaveLobby() {
    
    if(this.lobbyStore != null){
      this.lobbyStore.deleteLobby();
    }
    if(this.lobbyChannel != null){
      this.lobbyChannel.leave();
    }
    
  }

}
