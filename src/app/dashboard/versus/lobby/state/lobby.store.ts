import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { LobbyState } from './lobby.model';
import * as storage from '../../../../storage';
import { FiveStats } from 'src/app/models/fives-stats.model';
import { FivesUsers } from '../fives/fives.model';

export function createInitialState(): LobbyState {
  return {
    match_finded: false,
    lobby_token: null,
    result: null,
    total_bet: null,
    gamemode: null,
    map_selected: '',
    team1: null,
    team2: null,
    messages: [],
    presences: {},
    ip: null,
    ...storage.getLobby()
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'lobby' })
export class LobbyStore extends Store<LobbyState> {

  constructor() {
    super(createInitialState());
    
  }
  matchEnded(payload) {
    let lobby = storage.getLobby();
    const result: FiveStats = {
      team1: payload.stats.team1,
      team2: payload.stats.team2,
      team1_score: payload.match[0].team1score,
      team2_score: payload.match[0].team2score,
      winner: payload.winner
    }
    lobby.result = result;
    this.updateLobby(lobby);
  }

  soloTestEnd(){
    let lobby = storage.getLobby();
    const result: FiveStats = {
      team1: null,
      team2: null,
      team1_score: 16,
      team2_score: 14,
      winner: null
    }
    lobby.result = result;
    this.updateLobby(lobby);
  }

  showMatch(payload){
    let lobby = storage.getLobby();
    lobby.gamemode = "fives"
    const result: FiveStats = {
      team1: payload.stats.team1,
      team2: payload.stats.team2,
      team1_score: payload.match_result[0].team1score,
      team2_score: payload.match_result[0].team2score,
      winner: payload.winner
    }
    lobby.match_finded = true;
    lobby.result = result;
    this.updateLobby(lobby);
  }

  set_teams(team1: FivesUsers[], team2: FivesUsers[]){
    let lobby = storage.getLobby();
    lobby.team1 = team1;
    lobby.team2 = team2;
    this.updateLobby(lobby);
  }

  appendAllChatMessage(payload) {
    let lobby = storage.getLobby();
    lobby.messages.push(payload);
    this.updateLobby(lobby);
  }

  set_gamemode(mode) {
    let lobby = storage.getLobby();
    lobby.gamemode = mode;
    this.updateLobby(lobby);
  }

  set_ip(payload) {
    let lobby = storage.getLobby();
    lobby.ip = `connect ${payload.server}:27015`
    this.updateLobby(lobby);
  }

  set_map(payload) {
    let lobby = storage.getLobby();
    lobby.map_selected = payload.map;
    this.updateLobby(lobby);
  }

  updateLobby(lobby: LobbyState) {
    this.update(lobby);
    storage.saveLobby(lobby);
  }

  deleteLobby() {
    let lobby = storage.getLobby();
    lobby.match_finded = false;
    lobby.lobby_token = null;
    lobby.result = null;
    lobby.total_bet = null;
    lobby.team1 = null;
    lobby.team2 = null;
    lobby.messages = [];
    lobby.presences = {};
    lobby.ip = null;
    storage.saveLobby(lobby);
    this.updateLobby(lobby);
  }

}

