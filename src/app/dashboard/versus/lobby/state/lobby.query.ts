import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { LobbyStore } from './lobby.store';
import { LobbyState } from './lobby.model';
import { toBoolean } from '@angular-mdc/web';

@Injectable({ providedIn: 'root' })
export class LobbyQuery extends Query<LobbyState> {
  matchFinded$  = this.select(state => state.match_finded );
  lobby_token$  = this.select(state => state.lobby_token  );
  ip$           = this.select(state => state.ip           );
  ip_exists$    = this.select(state => toBoolean(state.ip));
  result$       = this.select(state => state.result       );
  total_bet$    = this.select(state => state.total_bet    );
  presences$    = this.select(state => state.presences    );
  team$         = this.select(state => state.team         );
  messages$     = this.select(state => state.messages     );
  gamemode$     = this.select(state => state.gamemode     );
  team1$        = this.select(state => state.team1);
  team2$        = this.select(state => state.team2);
  teams$        = this.select(state => [state.team1, state.team2]);
  map_selected$ = this.select(state => state.map_selected );
  map_ended$    = this.select(state => toBoolean(state.result));

  constructor(protected store: LobbyStore) {
    super(store);
  }

}
