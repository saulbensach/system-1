import { Component, OnInit, Input } from '@angular/core';
import { LobbyQuery } from './state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit {

  @Input() gameMode: string;

  result$: Observable<any>;
  winner$: Observable<any>;
  total_bet$: Observable<any>;
  ip$: Observable<any>;
  presences$: Observable<any>;
  gamemode$: Observable<any>;
  map_ended$: Observable<any>;

  constructor(
    private lobbyQuery: LobbyQuery
  ) { 
    this.result$ = this.lobbyQuery.result$;
    this.total_bet$ = this.lobbyQuery.total_bet$;
    this.ip$ = this.lobbyQuery.ip$;
    this.presences$ = this.lobbyQuery.presences$;
    this.gamemode$ = this.lobbyQuery.gamemode$;
    this.map_ended$ = this.lobbyQuery.map_ended$;
  }

  ngOnInit() {
    
  }

}
