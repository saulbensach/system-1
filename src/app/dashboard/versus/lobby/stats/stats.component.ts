import { Component, OnInit, ViewChild, ElementRef, ViewChildren } from '@angular/core';
import { MdcTabActivatedEvent} from '@angular-mdc/web';
import { LobbyService, LobbyQuery } from '../state';
import { Observable } from 'rxjs';
import { SessionQuery } from 'src/app/dashboard/state/session';
import { VersusQuery } from '../../state';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {

  /**
   * Falta guardar el estado del contador por si le dan F5 ETC
   */

  @ViewChild('scrollMe') scroll: ElementRef;

  tabIndex: number;
  ellipsis: boolean;
  ifMapSelected: boolean;
  messages$: Observable<any>;
  total_bet$: Observable<any>;
  ip_exists$: Observable<any>;
  ip$: Observable<any>;
  bet$: Observable<any>;
  map_selected$: Observable<any>;
  game_mode: string;
  id: number;
  counter: boolean[] = new Array();

  mapsLeftName = [
    {selected: false, name: "inferno", label: "Inferno"},
    {selected: false, name: "train", label: "Train"},
    {selected: false, name: "mirage", label: "Mirage"},
    {selected: false, name: "overpass", label: "Overpass"}
  ];
  mapsRightName = [
    {selected: false, name: "nuke", label: "Nuke"},
    {selected: false, name: "dust2", label: "Dust II"},
    {selected: false, name: "cache", label: "Cache"},
    {selected: false, name: "cobblestone", label: "Cobblestone"}
  ];

  serverStatus = [ //Frases para mostrar mientras se termina de configurar el server
    {label: "Configuring server", ellipsis: true},
    {label: "Taking a nap", ellipsis: true},
    {label: "Generating terrain", ellipsis: true},
    {label: "Ready Player One", ellipsis: false},
    {label: "Loading lines in 2016 LUL", ellipsis: false},
    {label: "Is this thing on?", ellipsis: false},
    {label: "Server is drunk, waking him up", ellipsis: true},
    {label: "ULTIMATE IS READY!", ellipsis: false},
    {label: "Insert Coin to Continue", ellipsis: false},
    {label: "Entering cheat codes", ellipsis: true},
    {label: "Rushing B", ellipsis: false},
    {label: "Pressing random buttons", ellipsis: false},
    {label: "Resurrecting dead memes", ellipsis: false}
  ];

  map: string;

  timePassed: boolean = false;

  interval: any;
  //vago extremo por escribir 
  a: number;

  //Prueba mostrar carga partida
  cont: number;
  waitingIp: string;


  constructor(
    private lobbyService: LobbyService,
    private lobbyQuery: LobbyQuery,
    private sessionQuery: SessionQuery,
    private versusQuery: VersusQuery
  ) { 
    this.ifMapSelected = false;
    this.a = -1;
    this.cont = this.randomIntFromInterval();
    this.ip$ = this.lobbyQuery.ip$;
    this.total_bet$ = this.lobbyQuery.total_bet$;
    this.messages$ = this.lobbyQuery.messages$;
    this.ip_exists$ = this.lobbyQuery.ip_exists$;
    this.bet$ = this.versusQuery.bet$;
    this.map_selected$ = this.lobbyQuery.map_selected$;
    this.lobbyQuery.map_selected$.subscribe(result => {
      if(result == "") {
        this.ifMapSelected =  true;
      } else{
        this.ifMapSelected = true;
      }
    });
    this.sessionQuery.id$.subscribe(id => {
      this.id = id;
    });
    this.lobbyQuery.gamemode$.subscribe(mode => {
      if(mode != null){
        this.game_mode = mode;
      }
    })
    for(let i = 0; i < 10; i++) {
      this.counter[i] = true;
    }
  }


  ngOnInit() {
    this.waitingIp = this.serverStatus[this.cont++].label;
    this.interval = setInterval(() => {
      if(this.a >= 0) {
        this.counter[this.a++] = false;
        if(this.a == 10){
          //enviar al backend el mapa enviado
          this.timePassed = true;
          this.lobbyService.sendMapSelected(this.map);
          clearInterval(this.interval);
        }
      } else {
        this.a++;
      }
      
    }, 1000);

    setInterval(() => {
      this.waitingIp = this.serverStatus[this.cont].label;
      this.cont = this.randomIntFromInterval();
      if (this.serverStatus[this.cont].ellipsis){
        this.ellipsis = true;
      } else {
        this.ellipsis = false;
      }
    }, 5000);

  }
  randomIntFromInterval() {
    return Math.floor(Math.random()*(13)+0);
  }

  mapSelected(map: string) {
    if(!this.timePassed) {
      for(let i = 0; i < this.mapsLeftName.length; i++) {
        this.mapsLeftName[i].selected = false;
        this.mapsRightName[i].selected = false;
        if(this.mapsLeftName[i].name == map){
          this.map = map;
          this.mapsLeftName[i].selected = true;
        } else if (this.mapsRightName[i].name == map) {
          this.map = map;
          this.mapsRightName[i].selected = true;
        }
      }
    }
    
  }

  ngOnDestroy() {
    
  }

  logTab(event: MdcTabActivatedEvent): void {
    this.tabIndex = event.index;
  }
  
  ngAfterViewChecked() {        
    this.scrollToBottom();        
  }

  ngAfterViewInit() {    
  }
  
  scrollToBottom() {
    try {
      this.scroll.nativeElement.scrollTop = this.scroll.nativeElement.scrollHeight;
    }catch(err) {}
  }


  sendText(value: string) {
    if(value.length > 1 && value != " "){
      this.lobbyService.sendAllChat(value);
    }
    
  }

}
