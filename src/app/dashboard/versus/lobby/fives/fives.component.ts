import { Component, OnInit } from '@angular/core';
import { MdcSnackbar } from '@angular-mdc/web';
import { LobbyQuery } from '../state';
import { Observable } from 'rxjs';
import { FivesUsers } from './fives.model';

@Component({
  selector: 'app-fives',
  templateUrl: './fives.component.html',
  styleUrls: ['./fives.component.scss']
})
export class FivesComponent implements OnInit {

  team1: FivesUsers[];
  team2: FivesUsers[];

  constructor(
    private snackbar: MdcSnackbar,
    private lobbyQuery: LobbyQuery
    ) { 
    }

  dismissIcon() {
    const snackbarRef = this.snackbar.open(`Friend request sent.`, 'Close', {
      dismiss: true
    });
  
    snackbarRef.afterDismiss().subscribe(reason => {
      
    });
  }

  ngOnInit() {
    this.lobbyQuery.teams$.subscribe(teams => {
      if(teams != null){
        if(teams != []){
          this.team1 = teams[0];
          this.team2 = teams[1];
        }
      }
    });
  }

}
