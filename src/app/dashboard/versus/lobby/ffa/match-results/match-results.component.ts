import { Component, OnInit } from '@angular/core';
import { MdcSnackbar } from '@angular-mdc/web';
import { MatchResult } from './match-result.model';

@Component({
  selector: 'app-match-results',
  templateUrl: './match-results.component.html',
  styleUrls: ['./match-results.component.scss']
})
export class MatchResultsComponent implements OnInit {

  //Los datos que le llegan son una lista ordenada del ganador al perdedor
  first: MatchResult;
  second: MatchResult;
  third: MatchResult;
  data: MatchResult[] = [
    {position: "1", img: null, username: "pedro123", tokens: "100", kills: "40", assists: "2", deaths: "10", headshots: "35"},
    {position: "2", img: null, username: "manolo32", tokens: "30", kills: "20", assists: "2", deaths: "10", headshots: "20"},
    {position: "3", img: null, username: "saulbensach", tokens: "10", kills: "18", assists: "2", deaths: "10", headshots: "17"},
    {position: "4", img: null, username: "hyphen", tokens: "0", kills: "17", assists: "2", deaths: "10", headshots: "5"},
    {position: "5", img: null, username: "jsos", tokens: "0", kills: "16", assists: "2", deaths: "10", headshots: "5"},
    {position: "6", img: null, username: "Gomez95", tokens: "0", kills: "16", assists: "2", deaths: "10", headshots: "5"},
    {position: "7", img: null, username: "Alcazar23", tokens: "0", kills: "15", assists: "2", deaths: "10", headshots: "10"},
    {position: "8", img: null, username: "gomez_spain", tokens: "0", kills: "14", assists: "2", deaths: "10", headshots: "3"},
    {position: "9", img: null, username: "tonijo", tokens: "0", kills: "13", assists: "2", deaths: "10", headshots: "2"},
    {position: "10", img: null, username: "roman226", tokens: "0", kills: "13", assists: "2", deaths: "10", headshots: "0"},
    {position: "11", img: null, username: "ih8tomatoes", tokens: "0", kills: "13", assists: "2", deaths: "10", headshots: "5"},
    {position: "12", img: null, username: "wsupbro", tokens: "0", kills: "12", assists: "2", deaths: "10", headshots: "1"},
    {position: "13", img: null, username: "interface", tokens: "0", kills: "12", assists: "2", deaths: "10", headshots: "2"},
    {position: "14", img: null, username: "object_96", tokens: "0", kills: "12", assists: "2", deaths: "10", headshots: "3"},
    {position: "15", img: null, username: "imnotworth", tokens: "0", kills: "2", assists: "2", deaths: "10", headshots: "0"},
    {position: "16", img: null, username: "isux", tokens: "50", kills: "0", assists: "2", deaths: "10", headshots: "0"}
  ];

  constructor(private snackbar: MdcSnackbar) {
    this.first = this.data.shift();
    this.second = this.data.shift();
    this.third = this.data.shift();
  }

  dismissIcon() {
    const snackbarRef = this.snackbar.open(`Friend request sent.`, 'Retry', {
      dismiss: true
    });
  
    snackbarRef.afterDismiss().subscribe(reason => {
      
    });
  }

  ngOnInit() {
    
  }

}
