export interface MatchResult {
    position: string;
    img: string, 
    username: string;
    tokens: string;
    kills: string;
    assists: string; 
    deaths: string;
    headshots: string;
}