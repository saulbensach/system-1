import { Component, OnInit } from '@angular/core';
import { MdcSnackbar } from '@angular-mdc/web';

@Component({
  selector: 'app-ffa',
  templateUrl: './ffa.component.html',
  styleUrls: ['./ffa.component.scss']
})
export class FfaComponent implements OnInit {
  constructor(private snackbar: MdcSnackbar) { }

  /*
    Fotos de perfil de los usuarios
    Bajas, Muertes, K/D Ratio,
  */

 dismissIcon() {
  const snackbarRef = this.snackbar.open(`Friend request sent.`, 'Retry', {
    dismiss: true
  });

  snackbarRef.afterDismiss().subscribe(reason => {
    
  });
}

  ngOnInit() {
  }

}
