import { Component, OnInit } from '@angular/core';
import { MdcSnackbar } from '@angular-mdc/web';

@Component({
  selector: 'app-ones',
  templateUrl: './ones.component.html',
  styleUrls: ['./ones.component.scss']
})
export class OnesComponent implements OnInit {

  constructor(private snackbar: MdcSnackbar) { }

  dismissIcon() {
    const snackbarRef = this.snackbar.open(`Friend request sent.`, 'Retry', {
      dismiss: true
    });
  
    snackbarRef.afterDismiss().subscribe(reason => {
      
    });
  }

  ngOnInit() {
  }

}
