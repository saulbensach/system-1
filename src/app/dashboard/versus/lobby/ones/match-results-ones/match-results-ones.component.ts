import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LobbyService, LobbyQuery } from '../../state';
import { PlayerStats } from 'src/app/models/player-stats.model';
import { ApiService } from 'src/app/shared/services/api.service';

@Component({
  selector: 'app-match-results-ones',
  templateUrl: './match-results-ones.component.html',
  styleUrls: ['./match-results-ones.component.scss']
})
export class MatchResultsOnesComponent implements OnInit {

  team1_score: number;
  team2_score: number;
  team1: PlayerStats[];
  team2: PlayerStats[];
  subscription: Subscription;
  winner: string;

  constructor(
    private lobbyQuery: LobbyQuery,
    private lobbyService: LobbyService,
    private api: ApiService
  ) { }

  ngOnInit() {
    this.subscription = this.lobbyQuery.result$.subscribe(result => {
      this.team1_score = result.team1_score;
      this.team2_score = result.team2_score;
      this.team1 = result.team1;
      this.team2 = result.team2;
      this.winner = result.winner; 
      this.team1 = this.team1.slice().sort((player1, player2) => {
        if(player1.kills < player2.kills) return 1;
        if(player1.kills > player2.kills) return -1;
      });
      this.team2 = this.team2.slice().sort((player1, player2) => {
        if(player1.kills < player2.kills) return 1;
        if(player1.kills > player2.kills) return -1;
      });

    });
  }

  endMatch(){
    this.subscription.unsubscribe();
    this.lobbyService.leaveLobby();
  }

}
