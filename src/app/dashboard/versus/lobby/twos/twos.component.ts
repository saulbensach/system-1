import { Component, OnInit } from '@angular/core';
import { MdcSnackbar } from '@angular-mdc/web';

@Component({
  selector: 'app-twos',
  templateUrl: './twos.component.html',
  styleUrls: ['./twos.component.scss']
})
export class TwosComponent implements OnInit {

  constructor(private snackbar: MdcSnackbar) { }

  dismissIcon() {
    const snackbarRef = this.snackbar.open(`Friend request sent.`, 'Retry', {
      dismiss: true
    });
  
    snackbarRef.afterDismiss().subscribe(reason => {
      
    });
  }

  ngOnInit() {
  }

}
