import { Team } from "./team.model";

export interface VersusState {
    searching: boolean;
    bet: string;
    loading: boolean;
    mode: string;
    askher: boolean;
    ask_accepted: boolean;
    users_in: number;
    users_out: number;
    region: string;
    team: Team
}