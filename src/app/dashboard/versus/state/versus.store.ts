import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import * as storage from '../../../storage';
import { VersusState } from './versus.model';

export function createInitialState(): VersusState {
  return {
    searching: false,
    loading: false,
    bet: null,
    mode: null,
    askher: false,
    users_in: null,
    users_out: null,
    region: null,
    ask_accepted: false,
    team: null,
    ...storage.getVersus()
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'versus' })
export class VersusStore extends Store<VersusState> {

  constructor() {
    super(createInitialState());
  }

  updateVersus(versus: VersusState){
    this.update(versus);
    storage.saveVersus(versus);
  }

  setBet(bet: string) {
    let versus = storage.getVersus();
    versus.bet = bet;
    this.updateVersus(versus);
  }

  setRegion(region: string){
    let versus = storage.getVersus();
    versus.region = region;
    this.updateVersus(versus);
  }

  clearRegion(region: string){
    let versus = storage.getVersus();
    versus.region = null;
    this.updateVersus(versus);
  }

  acceptAsk(){
    let versus = storage.getVersus();
    versus.ask_accepted = true;
    this.updateVersus(versus);
  }

  searching(searching: boolean, params?) {
    let versus = storage.getVersus();
    if(params != null){
      versus.bet = params.bet;
      versus.mode = params.mode;
    }
    versus.searching = searching;
    this.updateVersus(versus);
  }

  exit_team(){
    let versus = storage.getVersus();
    versus.team = null;
    this.updateVersus(versus);
  }

  set_team_code(code){
    let versus = storage.getVersus();
    let team = versus.team;
    if(team == null) {
      team = {
        team: [],
        leader: null,
        code: code
      }
    }
    team.code = code;
    versus.team = team;
    this.updateVersus(versus);
  }

  create_team(team) {
    let versus = storage.getVersus();
    versus.team = team;
    this.updateVersus(versus);
  }

  add_player_team(player) {
    let versus = storage.getVersus();
    let team = versus.team;
    if(team == null) {
      team = {
        team: [],
        leader: null,
        code: null
      }
    }
    team.team.push(player);
    versus.team = team;
    this.updateVersus(versus);
  }

  update_wanna_play(payload) {
    let versus = storage.getVersus();
    versus.askher = true;
    versus.users_in = payload.acepted;
    versus.users_out = payload.not_acepted;
    this.updateVersus(versus);
  }

  close_wanna_play(){
    let versus = storage.getVersus();
    versus.askher = false;
    versus.users_in = -1;
    versus.users_out = -1;
    this.updateVersus(versus);
  }
  
  wanna_play(payload) {
    let versus = storage.getVersus();
    versus.users_in = payload.acepted;
    versus.users_out = payload.not_acepted;
    versus.askher = true;
    versus.ask_accepted = false;
    this.updateVersus(versus);
  }

  false_wanna_play() {
    let versus = storage.getVersus();
    versus.users_in = 5;
    versus.users_out = 5;
    versus.askher = true;
    this.updateVersus(versus);
  }

  closeShit() {
    let versus = storage.getVersus();
    versus.askher = false;
    this.updateVersus(versus);
  }

  loadingTeam(value){
    let versus = storage.getVersus();
    versus.loading = value;
    this.updateVersus(versus);
  }

  stopSearch() {
    let versus = storage.getVersus();
    versus.searching = false;
    versus.askher = false;
    versus.users_in = null;
    versus.users_out = null;
    versus.ask_accepted = false;
    this.updateVersus(versus);
  }

  close() {
    this.stopSearch();
  }

}

