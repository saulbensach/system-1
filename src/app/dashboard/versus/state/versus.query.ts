import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { VersusStore } from './versus.store';
import { VersusState } from './versus.model';

@Injectable({ providedIn: 'root' })
export class VersusQuery extends Query<VersusState> {
  searching$ = this.select(state => state.searching);
  loading$ = this.select(state => state.loading);
  bet$ = this.select(state => state.bet);
  askher$ = this.select(state => state.askher);
  users_in$ = this.select(state => state.users_in);
  users_out$ = this.select(state => state.users_out);
  team$ = this.select(state => state.team);
  ask_state = this.select(state => [state.users_in, state.users_out]);
  ask_accepted = this.select(state => state.ask_accepted);
  region$ = this.select(state => state.region);

  constructor(protected store: VersusStore) {
    super(store);
  }

}
