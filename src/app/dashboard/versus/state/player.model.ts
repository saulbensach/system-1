export interface Player {
    id: number;
    username: string;
    picture: string;
}