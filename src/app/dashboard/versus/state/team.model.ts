import { Player } from "./player.model";

export interface Team {
    team: Player[],
    leader: Player;
    code: string;
}