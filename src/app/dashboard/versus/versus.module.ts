import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { versusRouting } from './versus.routing';
import { VersusComponent } from './versus.component';
import { 
  MdcButtonModule, 
  MdcFormFieldModule, 
  MdcRippleModule, 
  MdcRadioModule, 
  MdcTypographyModule,
  MdcTabBarModule,
  MdcLinearProgressModule,
  MdcElevationModule,
  MdcIconButtonModule,
  MdcListModule,
  MdcIconModule,
  MdcDialogModule,
  MdcTextFieldModule,
  MdcGridListModule, 
  MdcSnackbarModule, 
  MdcMenuModule,
  MdcSelectModule
} from '@angular-mdc/web';
import { DirectivesModule } from '../../shared/directives/directives.module';
import { LobbyComponent } from './lobby/lobby.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardDirectivesModule } from '../directives/dash-directives.module';
import { FfaComponent } from './lobby/ffa/ffa.component';
import { OnesComponent } from './lobby/ones/ones.component';
import { TwosComponent } from './lobby/twos/twos.component';
import { FivesComponent } from './lobby/fives/fives.component';
import { DialogsModule } from 'src/app/shared/dialogs/dialogs.module';
import { StatsComponent } from './lobby/stats/stats.component';
import { MatchResultsComponent } from './lobby/ffa/match-results/match-results.component';
import { MatchResultsFivesComponent } from './lobby/fives/match-results/match-results.component';
import { MatchResultsOnesComponent } from './lobby/ones/match-results-ones/match-results-ones.component';

@NgModule({
  imports: [
    CommonModule,
    versusRouting,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
    MdcButtonModule,
    MdcFormFieldModule,
    MdcRippleModule,
    MdcRadioModule,
    MdcTypographyModule,
    MdcTabBarModule,
    MdcLinearProgressModule,
    MdcElevationModule,
    MdcIconButtonModule,
    MdcListModule,
    DashboardDirectivesModule,
    MdcIconModule,
    MdcDialogModule,
    DialogsModule,
    MdcTextFieldModule,
    MdcGridListModule, 
    MdcSnackbarModule,
    MdcMenuModule,
    MdcSelectModule
  ],
  declarations: [
    VersusComponent, 
    LobbyComponent, 
    FfaComponent, 
    OnesComponent, 
    TwosComponent, 
    FivesComponent,
    StatsComponent,
    MatchResultsComponent,
    MatchResultsFivesComponent,
    MatchResultsOnesComponent
  ]
})
export class VersusModule {}