import { Routes, RouterModule } from "@angular/router";
import { HelpComponent } from "./help.component";
import { ModuleWithProviders } from "@angular/compiler/src/core";

export const helpRoutes: Routes = [
    {
        path: '',
        component: HelpComponent,
        data: {
            pageTitle: 'Help'
        }
    }
];

export const helpRouting: ModuleWithProviders = RouterModule.forChild(helpRoutes);