import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { helpRouting } from './help.routing';
import { HelpComponent } from './help.component';

@NgModule({
  imports: [
    CommonModule,
    helpRouting
  ],
  declarations: [HelpComponent]
})
export class HelpModule { }
