import { Routes, RouterModule } from "@angular/router";
import { ChatRoomComponent } from "./chat-room.component";
import { ModuleWithProviders } from "@angular/compiler/src/core";

export const chatRoutes: Routes = [
    {
        path: '',
        component: ChatRoomComponent,
        data: {
            pageTitle: 'Chat'
        }
    }
];

export const chatRouting: ModuleWithProviders = RouterModule.forChild(chatRoutes);