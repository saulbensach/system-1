import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { chatRouting } from './chat-room.routing';
import { ChatRoomComponent } from './chat-room.component';

@NgModule({
  imports: [
    CommonModule,
    chatRouting
  ],
  declarations: [ChatRoomComponent]
})
export class ChatRoomModule { }
