import { Routes, RouterModule } from "@angular/router";
import { ChallengesComponent } from "./challenges.component";
import { ModuleWithProviders } from "@angular/compiler/src/core";

export const challengesRoutes: Routes = [
    {
        path: '',
        component: ChallengesComponent,
        data: {
            pageTitle: 'Challenges'
        }
    }
];

export const challengesRouting: ModuleWithProviders = RouterModule.forChild(challengesRoutes);