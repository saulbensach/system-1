import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChallengesComponent } from './challenges.component';
import { challengesRouting } from './challenges.routing';

@NgModule({
  imports: [
    CommonModule,
    challengesRouting
  ],
  declarations: [ChallengesComponent]
})
export class ChallengesModule { }
