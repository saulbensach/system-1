import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { ArenastackModule } from '../../shared/arenastack.module';
import { homeRouting } from './home.routing';
import { MdcButtonModule, MdcListModule, MdcSliderModule, MdcTypographyModule, MdcElevationModule } from '@angular-mdc/web';
import { DirectivesModule } from '../../shared/directives/directives.module';


@NgModule({
  imports: [
    CommonModule,
    homeRouting,
    ArenastackModule,
    MdcButtonModule,
    MdcListModule,
    DirectivesModule, 
    MdcSliderModule, 
    MdcTypographyModule,
    MdcElevationModule
    
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
