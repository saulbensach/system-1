import { Component, OnInit, isDevMode } from '@angular/core';

import { Observable } from 'rxjs';
import { SessionQuery, SessionService } from '../state/session';
import { environment } from 'src/environments/environment';
import { MdcSliderChange, MdcDialog} from '@angular-mdc/web';
import { environmentProd } from 'src/environments/environment.prod';
import { QuickRefComponent } from "src/app/shared/dialogs/quick-ref/quick-ref.component";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  url: string;

  name$: Observable<any>;
  surname$: Observable<any>;
  balance$: Observable<any>;
  token$: Observable<any>;
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
  roadTitle: String;
  roadText: String;
  urlimg: String;
  twttr: any;

  roadmap:any[] = [
    {roadTitle:"actually", roadText: "We are on beta move the slider to know our next step ❯", urlimg: "/assets/img/dashboard/home/estamosenbeta.png"},
    {roadTitle:"bet with money", roadText: "You will be able to earn real money betting for your self", urlimg: "/assets/img/dashboard/home/dinero.png"},
    {roadTitle:"tournaments", roadText: "The best tournamets to prove you are the best", urlimg: "/assets/img/dashboard/home/torneos.png"},
    {roadTitle:"bans system", roadText: "To protect you, you will be able to decide who is banned", urlimg: "/assets/img/dashboard/home/ban.png"},
    {roadTitle:"new games", roadText: "We want the best for our users so we are going to add new games", urlimg: "/assets/img/dashboard/home/masjuegos.png"},
    {roadTitle:"suggestions", roadText: "If you have some suggestion go to our forum", urlimg: "/assets/img/dashboard/home/sugerencias.png"}

  ]; 

  constructor(
    private session: SessionService, 
    private sessionQuery: SessionQuery,
    private dialog: MdcDialog
    ) {
      this.countdown();

      /*this.dialog.open(QuickRefComponent, {
        escapeToClose: false,
        clickOutsideToClose: false,
        buttonsStacked: false,
        id: "quick-ref"
      });*/
    }

  ngOnInit() {
    if(isDevMode()){
      this.url = environment.steam_auth
    } else {
      this.url = environmentProd.steam_auth;
    }
    this.surname$ = this.sessionQuery.surname$;
    this.token$ = this.sessionQuery.token$;
    // @ts-ignore
    twttr.widgets.load();
    this.roadTitle = this.roadmap[0].roadTitle;
    this.roadText = this.roadmap[0].roadText;
    this.urlimg = this.roadmap[0].urlimg;
  }

  onInput(event: MdcSliderChange): void {
    
  }
  
  onChange(event: MdcSliderChange): void {
    this.roadTitle = this.roadmap[event.value].roadTitle;
    this.roadText = this.roadmap[event.value].roadText;
    this.urlimg = this.roadmap[event.value].urlimg;
  }

  disconnect() {
    this.session.logout();
  }

  get2D(num) {
    if( num.toString().length < 2 )
        return "0" + num;
    return num.toString();
}

  countdown(){
    const waitTime = 1000,
      minute = waitTime * 60,
      hour = minute * 60,
      day = hour * 24;

      let countDown = new Date('May 8, 2019 00:00:00').getTime(),
        x = setInterval(()=> {
          let now = new Date().getTime(),
              distance = countDown - now;

          this.days = this.get2D(Math.floor(distance / (day)));

          this.hours = this.get2D(Math.floor((distance % (day)) / (hour)));

          this.minutes = this.get2D((Math.floor((distance % (hour)) / (minute))));

          this.seconds = this.get2D((Math.floor((distance % (minute)) / waitTime)));

          if (distance < 0) {
            clearInterval(x);
            
          }
      }, waitTime);
  }
}
