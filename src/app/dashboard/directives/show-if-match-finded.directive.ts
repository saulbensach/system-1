import { Directive, Input, TemplateRef, ViewContainerRef } from "@angular/core";
import { Subscription } from "rxjs";
import { LobbyQuery } from "../versus/lobby/state";

@Directive({ selector: '[showIfMatchFinded]'} )
export class ShowIfMatchFindedDirective {
    subscription: Subscription;
    @Input() showIfMatchFinded: boolean;

    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private lobbyQuery: LobbyQuery
    ) {}

    ngOnInit() {
        this.subscription = this.lobbyQuery.matchFinded$.subscribe(isMatchFinded => {
            this.viewContainer.clear();
            if(isMatchFinded){
                if(this.showIfMatchFinded) {
                    this.viewContainer.createEmbeddedView(this.templateRef);
                } else {
                    this.viewContainer.clear();
                }
            } else {
                if(this.showIfMatchFinded) {
                    this.viewContainer.clear();
                } else {
                    this.viewContainer.createEmbeddedView(this.templateRef);
                }
            }
        })
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}