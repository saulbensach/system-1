import { NgModule } from "@angular/core";
import { ShowIfMatchFindedDirective } from "./show-if-match-finded.directive";
import { CommonModule } from "@angular/common";
import { ShowIfSearchingDirective } from "./show-if-searching.directive";

@NgModule({
    imports: [
        CommonModule
    ],
    exports: [ShowIfMatchFindedDirective, ShowIfSearchingDirective],
    declarations: [
        ShowIfMatchFindedDirective, 
        ShowIfSearchingDirective
    ]
})
export class DashboardDirectivesModule {}