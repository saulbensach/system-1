import { Directive, Input, TemplateRef, ViewContainerRef } from "@angular/core";
import { Subscription } from "rxjs";
import { VersusQuery } from "../versus/state";

@Directive({ selector: '[showIfSearching]'})
export class ShowIfSearchingDirective {
    subscription: Subscription;
    @Input() showIfSearching: boolean;

    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private versusQuery: VersusQuery
    ) { }

    ngOnInit() {
        this.subscription = this.versusQuery.searching$.subscribe(isSearching => {
            this.viewContainer.clear();
            if(isSearching) {
                if(this.showIfSearching) {
                    this.viewContainer.createEmbeddedView(this.templateRef);
                } else {
                    this.viewContainer.clear();
                }
            } else {
                if(this.showIfSearching) {
                    this.viewContainer.clear();
                } else {
                    this.viewContainer.createEmbeddedView(this.templateRef);
                }
            }
        })
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}