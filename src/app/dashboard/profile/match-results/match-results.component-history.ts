import { Component, OnInit } from '@angular/core';
import { PlayerStats } from 'src/app/models/player-stats.model';
import { ApiService } from 'src/app/shared/services/api.service';
import { Subscription } from 'rxjs';
import { ProfileQuery, ProfileService } from 'src/app/dashboard/profile/state';
import { HistoryStats } from '../history/history.component';

@Component({
  selector: 'app-match-results-fives-history',
  templateUrl: './match-results.component-history.html',
  styleUrls: ['./match-results.component-history.scss']
})
export class MatchResultsFivesHistoryComponent implements OnInit {

  team1_score: number;
  team2_score: number;
  team1: PlayerStats[];
  team2: PlayerStats[];
  subscription: Subscription;
  winner: string;

  constructor(
    private profileQuery: ProfileQuery,
    private profileService: ProfileService,
    private api: ApiService
  ) { }

  ngOnInit() {
    this.subscription = this.profileQuery.showStats$.subscribe(showMatch => {
      if(showMatch){
        let id = this.profileQuery.getSnapshot().matchToShow;
        this.api.getHistory(id).subscribe((result: HistoryStats) => {
          this.team1_score = result.team1_score;
          this.team2_score = result.team2_score;
          //@ts-ignore
          this.team1 = result.stats.team1;
          //@ts-ignore
          this.team2 = result.stats.team2;
          this.winner = result.winner; 
          this.team1 = this.team1.slice().sort((player1, player2) => {
            if(player1.kills < player2.kills) return 1;
            if(player1.kills > player2.kills) return -1;
          });
          this.team2 = this.team2.slice().sort((player1, player2) => {
            if(player1.kills < player2.kills) return 1;
            if(player1.kills > player2.kills) return -1;
          });
          
        })
      }
    })
    
  }

  returnProfile(){
    this.profileService.clearHistoryMatch();
    this.subscription.unsubscribe();
  }

}
