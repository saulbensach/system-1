import { Component, OnInit, ViewChild } from '@angular/core';
import { MdcTabActivatedEvent, MdcTabBar } from '@angular-mdc/web';
import { Observable } from 'rxjs';
import { SessionQuery } from '../state/session';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api.service';
import { ProfileService, ProfileQuery } from './state';
import { ProfileState } from './state/profile.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @ViewChild('profiletabs') tabs: MdcTabBar;

  profileTabIndex: number;
  profilePic$: Observable<any>;
  banner$: Observable<any>;
  showStats$: Observable<boolean>;
  correctuser: boolean;
  url: String;
  
  constructor(
    private sessionQuery: SessionQuery,
    private route: ActivatedRoute,
    private profileService: ProfileService,
    private profileQuery: ProfileQuery,
    private api: ApiService,
    private router: Router
  ) {
    this.showStats$ = this.profileQuery.showStats$;
    this.profilePic$ = this.profileQuery.picture$;
    this.banner$ = this.profileQuery.banner$;
    this.route.params.subscribe(params => {
      this.sessionQuery.username$.subscribe(username => {
        this.correctuser = false;
        this.url = this.router.routerState.snapshot.url.split("/").pop();
        if (this.url === "settings" && username != params["name"]) {
          this.profileTabIndex = 0;
          this.tabs.activateTab(0);
          this.router.navigate(['profile', username]);
        }else{
          if(username == params["name"]){
            let user = this.sessionQuery.getSnapshot();
            this.correctuser = true;
            const profState: ProfileState = {
              username: user.username,
              picture: user.picture,
              banner: user.banner,
              id: user.id,
              showStats: false,
              matchToShow: -1,
              friends: user.friends
            }
            this.profileTabIndex = 0;
            if(this.tabs != undefined) {
              this.tabs.activateTab(0);
            }
            
            this.profileService.buildProfile(profState);
          } else {
            this.api.getUser(params["name"]).subscribe(res => {
              this.profileService.buildProfile(res);
            })
            if(this.tabs != undefined) {
              this.tabs.activateTab(0);
            }
            this.profileTabIndex = 0;
          }
        }
      })
      // Una vez obtenido el nombre del usuario, determinar si es el mismo o es otro usuario
      // Si es el mismo obtener los datos del localstorage se supone que se los pasamos en el login
      // en caso contrario hacer query a la api rest
    });
  }

  logTab(event: MdcTabActivatedEvent): void {
    this.profileTabIndex = event.index;
  }

  ngOnInit() {
  }

}
