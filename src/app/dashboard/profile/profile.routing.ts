import { Routes, RouterModule } from "@angular/router";
import { ProfileComponent } from "./profile.component";
import { ModuleWithProviders } from "@angular/compiler/src/core";
import { OverviewComponent } from "./overview/overview.component";
import { StatsComponent } from "./stats/stats.component";
import { ClipsComponent } from "./clips/clips.component";
import { HistoryComponent } from "./history/history.component";
import { FriendsComponent } from "./friends/friends.component";
import { SettingsComponent } from "./settings/settings.component";

export const profileRoutes: Routes = [
    {
        path: '',
        component: ProfileComponent,
        children: [
            {
                path: '', redirectTo: 'overview', pathMatch: 'full'
            },
            {
                path: 'overview',
                component: OverviewComponent
            },
            {
                path: 'stats',
                component: StatsComponent
            },
            {
                path: 'clips',
                component: ClipsComponent
            },
            {
                path: 'history',
                component: HistoryComponent
            },
            {
                path: 'friends',
                component: FriendsComponent
            },
            {
                path: 'settings',
                component: SettingsComponent
            }
        ]
    }
];

export const profileRouting: ModuleWithProviders = RouterModule.forChild(profileRoutes);