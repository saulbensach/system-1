import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { profileRouting } from './profile.routing';
import { MdcTabBarModule, MdcIconModule, MdcIconButtonModule, MdcImageListModule, MdcTypographyModule, MdcTextFieldModule, MdcButtonModule, 
  MdcElevationModule, 
  MdcDialogModule} from '@angular-mdc/web';
import { OverviewComponent } from './overview/overview.component';
import { StatsComponent } from './stats/stats.component';
import { SettingsComponent } from './settings/settings.component';
import { FriendsComponent } from './friends/friends.component';
import { ClipsComponent } from './clips/clips.component';
import { HistoryComponent } from './history/history.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { MatchResultsFivesHistoryComponent } from './match-results/match-results.component-history';

@NgModule({
  imports: [
    CommonModule,
    profileRouting,
    MdcTabBarModule,
    MdcIconModule,
    MdcIconButtonModule,
    MdcImageListModule,
    MdcTypographyModule,
    MdcTextFieldModule,
    MdcButtonModule,
    MdcElevationModule,
    DirectivesModule,
    MdcDialogModule
  ],
  declarations: [
    ProfileComponent, 
    OverviewComponent, 
    StatsComponent, 
    SettingsComponent, 
    HistoryComponent, 
    FriendsComponent, 
    ClipsComponent,
    MatchResultsFivesHistoryComponent
  ]
})
export class ProfileModule { }
