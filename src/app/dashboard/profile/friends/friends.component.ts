import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileQuery } from '../state';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit, OnDestroy {

  friends: any;

  constructor(
    private profileQuery: ProfileQuery,
    private router: Router
  ) { 
    this.profileQuery.friends$.subscribe(friends_sub => {
      
      if(friends_sub != null){
        this.friends = [];
        for(let i = 0; i < friends_sub.length; i++) {
          if(friends_sub[i].status == 2){
            this.friends.push(friends_sub[i]);
          }
        }
      }
    });
  }

  goProfile(name) {
    this.router.navigate(['profile', name])
  }

  ngOnInit() {
    
  }

  ngOnDestroy() {
  }

}
