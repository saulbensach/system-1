import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { ProfileState } from './profile.model';
import * as storage from '../../../storage';

export function createInitialState(): ProfileState {
  return {
    username: 'johndoe',
    picture: null,
    banner: null,
    id: -1,
    friends: [],
    showStats: false,
    matchToShow: -1,
    ...storage.getProfile()
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'profile' })
export class ProfileStore extends Store<ProfileState> {

  constructor() {
    super(createInitialState());
  }

  clearHistoryMatch(){
    let profile = storage.getProfile();
    profile.showStats = false;
    profile.matchToShow = -1;
    this.update(profile);
    storage.saveProfile(profile);
  }

  showMatch(id: number){
    let profile = storage.getProfile();
    profile.showStats = true;
    profile.matchToShow = id;
    this.update(profile);
    storage.saveProfile(profile);
  }

  create(profile: ProfileState){
    this.update(profile);
    storage.saveProfile(profile);
  }

  delete(){
    storage.clearProfile();
    this.update(createInitialState());
  }

}

