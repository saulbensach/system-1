import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { ProfileStore } from './profile.store';
import { ProfileState } from './profile.model';

@Injectable({ providedIn: 'root' })
export class ProfileQuery extends Query<ProfileState> {

  username$ = this.select(state => state.username);
  picture$ = this.select(state =>  state.picture);
  banner$ = this.select(state => state.banner);
  friends$ = this.select(state => state.friends);
  showStats$ = this.select(state => state.showStats);

  constructor(protected store: ProfileStore) {
    super(store);
  }

}
