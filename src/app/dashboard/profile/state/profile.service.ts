import { Injectable } from '@angular/core';
import { ProfileState } from './profile.model';
import { ProfileStore } from './profile.store';


@Injectable({ providedIn: 'root' })
export class ProfileService {

  constructor(
    private profileStore: ProfileStore
  ) {
  }

  showMatch(id: number){
    this.profileStore.showMatch(id);
  }

  clearHistoryMatch(){
    this.profileStore.clearHistoryMatch();
  }

  buildProfile(profile: ProfileState) {
    this.profileStore.create(profile);
  }  

}
