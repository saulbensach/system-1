import { Friend } from "src/app/models/friend.model";

export interface ProfileState {
    username: string;
    picture: string;
    banner: string;
    id: number;
    friends: Friend[];
    showStats: boolean;
    matchToShow: number;
}