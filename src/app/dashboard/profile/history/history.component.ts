import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { ActivatedRoute } from '@angular/router';
import { LobbyService } from '../../versus/lobby/state';
import { ProfileService } from '../state';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  history: HistoryStats[];

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private profileService: ProfileService
  ) { }

  ngOnInit() {
    this.history = [];
    this.api.getGameStats(this.route.snapshot.parent.params["name"]).subscribe(result => {
      if(result != null){
        //@ts-ignore
        this.history = result.stats.reverse();
      }
    })
  }

  showMatch(id){
    this.profileService.showMatch(id);
  }

}

export interface HistoryStats {
  winner: string;
  team2_score: number;
  team1_score: number;
  team: string;
  assists: number;
  mode: string;
  match_id: number;
  map: string;
  kills: number;
  headshots: number;
  deaths: number;
  bet:  number;
}
