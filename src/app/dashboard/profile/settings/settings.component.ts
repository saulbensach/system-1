import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';
import { MdcSnackbar, MdcTabBar, MdcDialog } from '@angular-mdc/web';
import { SessionService, SessionQuery } from '../../state/session';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageCropperComponent } from 'src/app/shared/dialogs/image-cropper/image-cropper.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  @ViewChild('profiletabs') tabs: MdcTabBar;

  settings_name: string;
  settings_surname: string;
  image_file: string | ArrayBuffer;
  banner_file: string | ArrayBuffer;
  
  constructor(
    private sessionQuery: SessionQuery,
    private api: ApiService,
    private snackbar: MdcSnackbar,
    private sessionService: SessionService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MdcDialog
  ) { }
  

  ngOnInit() {
    this.sessionQuery.username$.subscribe(username => {
      let url = this.router.routerState.snapshot.url.split("/").pop();
      if (url === "settings" && username != this.route.snapshot.parent.params["name"]) {
        this.router.navigate(['profile', username]);
        this.tabs.activateTab(0);
      }
    });
  }

  onSubmit() {
    
  }

  onChangeName(value: any): void {
    this.settings_name = value;
  }

  onChangeSurName(value: any): void {
    this.settings_surname = value;
  }

  changeName() {
    this.api.changeNameUser(this.settings_name).subscribe(res => {

    });
    const snackbarRef = this.snackbar.open(`Name changed.`, 'Retry', {
      dismiss: true
    });

    snackbarRef.afterDismiss().subscribe(reason => {
      
    });
  }

  changeBannerImage(){
    this.doneBannerImage(this.banner_file);
  }

  changeSurName() {
    this.api.changeSurNameUser(this.settings_surname).subscribe(res => {
      this.snackbar.open(`Surname changed.`);
    });
  }

  fileChangedImage(event) {
    this.dialog.open(ImageCropperComponent, {
      data: {event: event},
      escapeToClose: false,
      clickOutsideToClose: false,
      buttonsStacked: false
    });
  }

  fileChangedBanner(event) {
    let file = event.target.files[0];
    let fileReader = new FileReader();
    fileReader.onloadend = () => {
      this.banner_file = fileReader.result;
      //this.doneBannerImage(fileReader.result)
    }
    fileReader.readAsDataURL(file);
  }

  doneBannerImage(file){
    this.api.changeBannerProfile(file.split(',')[1]).subscribe(res => {
      let url = res["url"];
      let error = res["error"];

      if (error == undefined) {
        this.sessionService.changeUrlBanner(url);
        window.location.reload();
      } else {
        
      }
    });
  }

}
