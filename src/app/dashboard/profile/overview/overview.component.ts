import { Component, OnInit } from '@angular/core';
import { ProfileQuery } from '../state';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  linked: boolean; //Variable para saber si el usuario ha linkeado su red social
  username$: Observable<any>;

  friends: any;
  friends_aux: any;

  constructor(
    private profileQuery: ProfileQuery,
    private router: Router
  ) { 
    this.profileQuery.friends$.subscribe(friends_sub => {
      if(friends_sub != null){
        this.friends = [];
        this.friends_aux = [];
        for(let i = 0; i < friends_sub.length; i++) {
          if(friends_sub[i].status == 2){
            this.friends.push(friends_sub[i]);
            this.friends_aux.push(friends_sub[i]);
          }
        }
        if (this.friends.length > 3) {
          this.friends = [];
          for(let i = 0; i < 4; i++) {
            let pos = Math.floor(Math.random() * this.friends_aux.length) + 0;
            this.friends.push(this.friends_aux[pos]);
            this.friends_aux.splice(pos,1);
          }
        }
      }
    });
    
  }

  goProfile(name) {
    this.router.navigate(['profile', name])
  }

  ngOnInit() {
    this.username$ = this.profileQuery.username$;
    this.linked = false;
  }

}
