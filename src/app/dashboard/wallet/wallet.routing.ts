import { Routes, RouterModule } from "@angular/router";
import { WalletComponent } from "./wallet.component";
import { ModuleWithProviders } from "@angular/compiler/src/core";

export const walletRoutes: Routes = [
    {
        path: '',
        component: WalletComponent,
        data: {
            pageTitle: 'Wallet'
        }
    }
];

export const walletRouting: ModuleWithProviders = RouterModule.forChild(walletRoutes);