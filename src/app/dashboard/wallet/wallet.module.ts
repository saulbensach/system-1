import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { walletRouting } from './wallet.routing';
import { WalletComponent } from './wallet.component';

@NgModule({
  imports: [
    CommonModule,
    walletRouting
  ],
  declarations: [WalletComponent]
})
export class WalletModule { }
