import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {routing} from './app.routing';
import { AppComponent } from './app.component';
import { LayoutModule } from './shared/layouts/layout.module';
import { DirectivesModule } from './shared/directives/directives.module';
import { MdcDialogModule, MdcIconModule, MdcButtonModule } from '@angular-mdc/web';
import { ToastrModule } from 'ngx-toastr';
import { InviteComponent } from './shared/customs-toastrs/invite/invite.component';
import { FriendRequestComponent } from './shared/customs-toastrs/friend-request/friend-request.component';

@NgModule({
  declarations: [
    AppComponent,
    InviteComponent,
    FriendRequestComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routing,
    LayoutModule,
    DirectivesModule,
    MdcDialogModule,
    MdcIconModule,
    MdcButtonModule,
    ToastrModule.forRoot()
  ],
  exports: [],
  entryComponents: [InviteComponent, FriendRequestComponent], 
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
