const SESSION_KEY = 'session';
const VERSUS_KEY = 'versus';
const DASHBOARD_KEY = 'dashboard';
const SOCKET_KEY = 'socket';
const LOBBY_KEY = 'lobby';
const DEBUG_KEY = 'debug';
const DIALOG_KEY = 'dialog';
const PROFILE_KEY = 'profile';

//Todo arreglar y meterlo todo en 4 funciones o menos

export function getProfile(){
    const profile = localStorage.getItem(PROFILE_KEY);
    return profile ? JSON.parse(profile) : {};
}

export function saveProfile(profile) {
    localStorage.setItem(PROFILE_KEY, JSON.stringify(profile));
}

export function clearProfile() {
    localStorage.removeItem(PROFILE_KEY);
}

export function getDialog() {
    const dialog = localStorage.getItem(DIALOG_KEY);
    return dialog ? JSON.parse(dialog) : {};
}

export function saveDialog(dialog) {
    localStorage.setItem(DIALOG_KEY, JSON.stringify(dialog));
}

export function clearDialog(){
    localStorage.removeItem(DIALOG_KEY);
}

export function getLobby() {
    const lobby = localStorage.getItem(LOBBY_KEY);
    return lobby ? JSON.parse(lobby) : {};
}

export function saveLobby(lobby) {
    localStorage.setItem(LOBBY_KEY, JSON.stringify(lobby));
}

export function clearLobby() {
    localStorage.removeItem(LOBBY_KEY);
}

export function getSocket(){
    const socket = localStorage.getItem(SOCKET_KEY);
    return socket ? JSON.parse(socket) : {};
}

export function saveSocket(socket) {
    localStorage.setItem(SOCKET_KEY, JSON.stringify(socket));
}

export function clearSocket() {
    localStorage.removeItem(SOCKET_KEY);
}

export function getDashboard() {
    const dashboard = localStorage.getItem(DASHBOARD_KEY);
    return dashboard ? JSON.parse(dashboard) : {};
}

export function saveDashboard(dashboard) {
    localStorage.setItem(DASHBOARD_KEY, JSON.stringify(dashboard));
}

export function clearDashboard() {
    localStorage.removeItem(DASHBOARD_KEY);
}

export function getVersus() {
    const versus = localStorage.getItem(VERSUS_KEY);
    return versus ? JSON.parse(versus) : {};
}

export function saveVersus(versus) {
    localStorage.setItem(VERSUS_KEY, JSON.stringify(versus));
}

export function clearVersus(){
    localStorage.removeItem(VERSUS_KEY);
}

export function getSession() {
    const session = localStorage.getItem(SESSION_KEY);
    return session ? JSON.parse(session) : {};
}

export function saveSession(session) {
    localStorage.setItem(SESSION_KEY, JSON.stringify(session));
}

export function clearSession() {
    localStorage.removeItem(SESSION_KEY);
    clearVersus();
    clearLobby();
    clearDashboard();
    clearDialog();
    clearProfile();
    clearSocket();
}

export function getDebug() {
    const debug = localStorage.getItem(DEBUG_KEY);
    return debug ? JSON.parse(debug) : {};
}

export function saveDebug(debug) {
    localStorage.setItem(DEBUG_KEY, JSON.stringify(debug));
}