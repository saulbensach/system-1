export interface Notification {
  type: string;
  title: string;
  id: number;
  content: string;
}