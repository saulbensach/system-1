export interface PlayerStats {
    assists: number;
    deaths: number;
    headshots: number;
    kills: number;
    picture: string;
    team: string;
    user: string;
}