export interface Friend {
    friend_username: string;
    friend_user_id: number;
    status: number;
    profile: string;
    connected: boolean;
}