import { Notification } from './notification.model';

export interface Notifications {
  teamRequest: Notification[],
  friendRequests: Notification[],
  simpleNotification: Notification[],
  count: number;
}

