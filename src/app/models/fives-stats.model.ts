import { PlayerStats } from "./player-stats.model";

export interface FiveStats {
    team1: PlayerStats[],
    team2: PlayerStats[]
    team1_score: number;
    team2_score: number;
    winner: string;
}