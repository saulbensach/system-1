import { Component, NgZone } from '@angular/core';
import { environment } from '../environments/environment';
import { akitaDevtools } from '@datorama/akita';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent{
  title = 'ArenaStack';

  constructor(
    private ngZone: NgZone
    ){
    if(!environment.production){
      akitaDevtools(ngZone, undefined);
    }
  }

  ngOnInit() {
    
  }

}
