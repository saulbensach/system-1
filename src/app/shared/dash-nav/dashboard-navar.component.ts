import { Component, OnInit } from '@angular/core';
import { MdcListItem, MdcTabActivatedEvent, MdcSwitchChange, MdcSnackbar, MdcDialog, MdcDialogRef } from '@angular-mdc/web';
import { SessionQuery, SessionService, SessionStore } from 'src/app/dashboard/state/session';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { environment } from "../../../environments/environment";
import { ApiService } from '../services/api.service';
import { Notification } from '../../models/notification.model';
import { Friend } from 'src/app/models/friend.model';
import { Router } from '@angular/router';
import { DashboardService } from 'src/app/dashboard/state/dashboard';
import { SocketService } from 'src/app/dashboard/state/socket';
import { QuickRefComponent } from "src/app/shared/dialogs/quick-ref/quick-ref.component";
import { ProfileQuery } from 'src/app/dashboard/profile/state';

@Component({
  selector: 'app-dash-nav',
  templateUrl: './dashboard-navar.component.html',
  styleUrls: ['./dashboard-navar.component.scss']
})
export class DashboardNavbarComponent implements OnInit {
  notificationsTabs = [
    { icon: "announcement" },
    { icon: "check_circle" },
    { icon: "person" }
  ];

  notificationsInvitation: any;
  notificationsAnnouncements: Notification[];
  notificationsFriend: Notification[];
  notificationsAchievements: Notification[];
  notificationsCount: number;
  name$: Observable<any>;
  balance$: Observable<any>;
  friends: Friend[];
  Myfriends: any;
  notification$: Observable<any>;
  profilePic$: Observable<any>;
  searchInterval: any;
  doSearch: boolean;
  lastSelection: number;
  searchText: string;
  searchUserArray: SearchList[];
  tabIndex: number;
  senpai: FormGroup;

  //quitar el httpclient de aquí!
  constructor(
    private sessionQuery: SessionQuery,
    private sessionService: SessionService,
    private socketService: SocketService,
    private dashboardService: DashboardService,
    private sessionStore: SessionStore,
    private formBuilder: FormBuilder,
    private snackbar: MdcSnackbar,
    private router: Router,
    private api: ApiService,
    private profileQuery: ProfileQuery,
    private dialog: MdcDialog
  ) {
    this.notificationsCount = 0;
    this.tabIndex = 2;
    this.searchText = "";
    this.doSearch = false;
    this.notificationsCount = 0;
    this.notificationsInvitation = this.dashboardService.notifications;
    this.sessionQuery.friends$.subscribe(friend_sub => {
      if(friend_sub != null || friend_sub != undefined){
        this.friends = friend_sub;
        for(let i = 0; i < friend_sub.length; i++){
          if(friend_sub[i].status == 1){
            this.notificationsCount += 1;
          }
        }
        
      }
      
    });
    this.sessionQuery.notification$.subscribe(notifications => {
      if(notifications != null){
        this.notificationsFriend = notifications.friendRequests;
        this.notificationsAnnouncements = notifications.simpleNotification;
        
        if(this.notificationsFriend != undefined){
          this.notificationsCount += this.notificationsFriend.length
        }
        if(this.notificationsAnnouncements != undefined){
          this.notificationsCount += this.notificationsAnnouncements.length;
        }
        //this.notificationsFriend = notifications;
      }
    });
    this.profileQuery.friends$.subscribe(friends_sub => {
      if(friends_sub != null){
        this.Myfriends = [];
        for(let i = 0; i < friends_sub.length; i++) {
          if(friends_sub[i].status == 2){
            this.Myfriends.push(friends_sub[i]);
          }
        }
      }
    });
    this.searchInterval = setInterval(() => {
      if(this.doSearch) {
        this.api.getUserSearch(this.searchText).subscribe(res => {
          this.searchUserArray = [];
          for (let i = 0; i < res.payload.length; i++) {
            let temp_value = false;
            for (let j = 0; j < this.Myfriends.length; j++) {
              if (this.Myfriends[j].friend_username === res.payload[i].username) {
                temp_value = true;
                break;
              }
            }
            let temporal: SearchList = {
              picture: res.payload[i].profile,
              profile: res.payload[i].username,
              is_friend: temp_value,
              id: res.payload[i].id
            }
            this.searchUserArray.push(temporal);
          }
          //console.log(this.searchUserArray);
        })
        this.doSearch = false;
      } 
    }, 1000);
  }

  openInfo() {
    this.dialog.open(QuickRefComponent, {
      escapeToClose: false,
      clickOutsideToClose: false,
      buttonsStacked: false,
      id: "quick-ref"
    });
  }

  addFriend(friend){
    this.friends.push(friend);
  }

  goProfile(){
    this.sessionQuery.username$.subscribe(username => {
      this.router.navigate(['profile', username])
    });
  }

  goHome(){
    this.router.navigate(['home'])
  }

  /**Añadir algo para eliminar al amigo del array de friend_id */
  //Además mirar de cambiar el estado a 2 de forma local en sessionstorage y sessionservice añadir metodos para esto
  goSettings(){
    this.sessionQuery.username$.subscribe(username => {
      this.router.navigate([`profile/${username}/settings`])
    });
  }

  accept(friend_id) {
    this.api.acceptFriend(friend_id).subscribe(() => {
      this.notificationsCount -= 1;
      let tmp = this.sessionQuery.getSnapshot().friends;
      let friend = null;
      for(let i = 0; i < tmp.length; i++){
        if(tmp[i].friend_user_id == friend_id){
          friend = tmp[i];
          break;
        }
      }
      this.sessionService.changeFriendStatus(friend.friend_user_id, 2);
    });
  }

  decline(friend_id) {
    this.sessionService.removeFriend(friend_id);
    this.api.declineFriend(friend_id).subscribe(() => {
      this.sessionService.removeFriend(friend_id);
      this.notificationsCount -= 1;
      this.snackbar.open(`Request declined.`);
    });    
  
  }

  send(user_two_id: any) {
    //TODO poner cuando hay otro code distinto a 200
    this.api.sendUserFriendInvitation(user_two_id).subscribe(response => {
      
      this.snackbar.open('Request sended');
    });
  }

  ngOnInit() {
    this.name$ = this.sessionQuery.username$;
    this.balance$ = this.sessionQuery.balance$;
    this.notification$ = this.sessionQuery.notification$;
    this.profilePic$ = this.sessionQuery.picture$;
    this.senpai = this.formBuilder.group({
      login: [false],
      lobby: [false],
      lobby_type: new FormControl(''),

    });
    this.onChanges();
  }

  clearStorage() {
    localStorage.clear();
  }

  onChanges(): void {
    this.senpai.valueChanges.subscribe(value => {
      if(value.login){
        const session = {
          token: 'abcd',
          name: 'John',
          username: 'johndoe',
          surname: 'doe',
          picture: null,
          steamid: null,
          notifications: null,
          banner: null,
          friends: [],
          id: -1,
          balance: "100",
        };
        this.sessionStore.login(session)
      } else {
        this.sessionService.logout();
      }
      if(value.lobby) {

      }
      
    });
  }

  goToProfile(name) {
    this.router.navigate(['profile', name])
  }

  onMenuSelect(event: { index: number, item: MdcListItem }) {
    this.lastSelection = event.index;
  }

  logTab(event: MdcTabActivatedEvent): void {
    this.tabIndex = event.index;
  }

  onInputSearch(value: any): void {
    this.searchText = value;
    if(this.searchText.length >= 3) {
      this.doSearch = true;
    } else {
      this.doSearch = false;
    }
  }

  onChange(event: MdcSwitchChange) {
    
  }

  logOut() {
    this.dashboardService.closeAllChannels();
    this.sessionService.logout();
    this.socketService.disconnect();
    clearInterval(this.searchInterval);
  }
}

interface SearchList {
  picture: string;
  profile: string;
  is_friend: boolean;
  id: number;
}
