import { Directive, Input, TemplateRef, ViewContainerRef } from "@angular/core";
import { Subscription } from "rxjs";
import { SessionQuery } from "src/app/dashboard/state/session";

@Directive({ selector: '[showIfLoggedIn]'})
export class ShowIfLoggedInDirective {
    subscription: Subscription;
    @Input() showIfLoggedIn: boolean;

    constructor(
        private teamplateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private authQuery: SessionQuery
    ) {}

    ngOnInit() {
        this.subscription = this.authQuery.isLoggedIn$.subscribe(isLoggedIn => {
            this.viewContainer.clear();
            if(isLoggedIn) {
                if(this.showIfLoggedIn) {
                    this.viewContainer.createEmbeddedView(this.teamplateRef);
                } else {
                    this.viewContainer.clear();
                }
            } else {
                if(this.showIfLoggedIn) {
                    this.viewContainer.clear();
                } else {
                    this.viewContainer.createEmbeddedView(this.teamplateRef);
                }
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}