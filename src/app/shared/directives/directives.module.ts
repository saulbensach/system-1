import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowIfLoggedInDirective } from './show-if-logged-in.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [ShowIfLoggedInDirective],
  declarations: [ShowIfLoggedInDirective]
})
export class DirectivesModule { }
