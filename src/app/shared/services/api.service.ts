import { Injectable, isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { environmentProd } from '../../../environments/environment.prod';
import { Observable } from 'rxjs';
import { SessionQuery } from 'src/app/dashboard/state/session';
import { map } from 'rxjs/operators';
import { SessionState } from 'src/app/dashboard/state/session/session.model';
import { ProfileState } from 'src/app/dashboard/profile/state/profile.model';
import { HistoryStats } from 'src/app/dashboard/profile/history/history.component';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  headers: HttpHeaders;
  baseUrl: string;
  interval: any;

  constructor(
    private http: HttpClient,
    private sessionQuery: SessionQuery
  ) {
    this.sessionQuery.token$.subscribe(token => {
      if(token != null || token != "") {
        this.headers = new HttpHeaders()
        .set('Content-Type', 'application/json; charset=utf-8')
        .set('Authorization', `Bearer ${token}`);
        if(isDevMode()) {
          this.baseUrl = environment.base_url;
        } else {
          this.baseUrl = environmentProd.base_url;
        }
      }
      
    });
  }

  login(creds){
    const login_headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    let email = creds.email;
    let password = creds.password;
    return this.http.post(this.baseUrl + "sign_in", {email, password}, {headers: login_headers});
  }

  register(params, partner) {
    const register_headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    const name = params.name;
    const surname = params.surname;
    let data = params.birthday;
    const gender = params.gender;
    const currency = params.currency;
    const country = params.country;
    const username = params.username;
    const email = params.email;
    const password = params.password;
    const terms_of_service = params.terms_of_service;
    let day = data.substring(8,11);
    let month = data.substring(5,7);
    let year = data.substring(0,4);
    const birthday = `${day}/${month}/${year}`

    return this.http.post<SessionState>(
      this.baseUrl + "sign_up",
      {
        name, 
        surname, 
        birthday, 
        gender, 
        currency, 
        country, 
        username, 
        email, 
        password, 
        terms_of_service,
        partner
      }, {headers: register_headers}
    );
  }

  getUser(username: string): Observable<any> {
    const params = new HttpParams().set('name', username);
    return this.http.get<ProfileState>(this.baseUrl + "users", {headers: this.headers, params: params}).pipe(map(user => {return user;}));
  }

  getUserSearch(search: string): Observable<any> {
    const params = new HttpParams().set('name', search);
    return this.http.get(this.baseUrl + "search/users", {headers: this.headers, params: params}).pipe(map(this.extractData));
  }

  sendUserFriendInvitation(user_two_id: any): Observable<any> {
    return this.http.post(this.baseUrl + "friends", {user_two_id}, {headers: this.headers}).pipe(map(this.extractData));
  }

  changeNameUser(name: any): Observable<any> {
    return this.http.post(this.baseUrl + "profile/change_name", {name}, {headers: this.headers}).pipe(map(this.extractData));
  }

  changeSurNameUser(surname: any): Observable<any> {
    return this.http.post(this.baseUrl + "profile/change_surname", {surname}, {headers: this.headers}).pipe(map(this.extractData));
  }

  acceptFriend(user_action_id) {
    return this.http.post(this.baseUrl + "friends/accept_petition", {user_action_id}, {headers: this.headers}).pipe(map(this.extractData));
  }

  declineFriend(user_action_id){
    return this.http.post(this.baseUrl + "friends/decline_petition", {user_action_id}, {headers: this.headers}).pipe(map(this.extractData));
  }

  changeImageProfile(image: any) {
    return this.http.post(this.baseUrl + "images", {image}, {headers: this.headers}).pipe(map(this.extractData));
  }

  changeBannerProfile(image: any) {
    return this.http.post(this.baseUrl + "banners", {image}, {headers: this.headers}).pipe(map(this.extractData));
  }
  
  getGameStats(username: string){
    const params = new HttpParams().set('name', username);
    return this.http.get<HistoryStats>(this.baseUrl + "profile/game_stats", {headers: this.headers, params: params}).pipe(map(this.extractData));
  }

  getHistory(matchid: any){
    const params = new HttpParams().set('matchid', matchid);
    return this.http.get<HistoryStats>(this.baseUrl + "profile/history", {headers: this.headers, params: params}).pipe(map(this.extractData));
  }

  sendTokenEmail(path: string){
    return this.http.post(this.baseUrl + "confirmation", {path}, {headers: this.headers}).pipe(map(this.extractData));
  }

  sendResetEmail(email: string){
    return this.http.post(this.baseUrl + "forgot", {email}, {headers: this.headers}).pipe(map(this.extractData));
  }

  sendTokenNewPassword(path: string, password: string){
    return this.http.post(this.baseUrl + "renew_password", {path, password}, {headers: this.headers}).pipe(map(this.extractData));
  }

  private extractData(res: Response){
    let body = res;
    return body || { };
  }

}
