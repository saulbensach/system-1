import { Component, OnInit, Inject, ViewChild, ElementRef, Renderer2, AfterViewInit, AfterViewChecked } from '@angular/core';
import { MdcDialog, MdcDialogRef, MdcSwitchChange, MDC_DIALOG_DATA } from '@angular-mdc/web';
import { DashboardService } from 'src/app/dashboard/state/dashboard';
import { VersusQuery } from 'src/app/dashboard/versus/state';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-askher',
  templateUrl: './askher.component.html',
  styleUrls: ['./askher.component.scss']
})
export class AskherComponent implements OnInit {

  @ViewChild('container') container: ElementRef;

  pressed: boolean;
  usersIn: number;
  usersOut: number;
  who_acepted: number;
  versusPopup: String;
  askherType: string;
  clicked: Subject<any>;
  ask_accepted: Observable<boolean>;
  div: any;
  //TODO mover dashboard service a dialog service y hacer la llamada accept match a travbés de dialogservice!
  constructor(
    private dialogRef: MdcDialogRef<AskherComponent>,
    @Inject(MDC_DIALOG_DATA) data: any,
    private versusQuery: VersusQuery,
    private renderer: Renderer2
  ) { 
    this.div = null;
    this.usersIn = data.users_in;
    this.usersOut = data.users_out;
    this.askherType = data.askType;
    this.clicked = data.emitter;
    this.pressed = false;
    this.versusPopup = this.versusPopup;
    this.ask_accepted = this.versusQuery.ask_accepted;
  }


  ngOnInit() {
    this.versusQuery.ask_state.subscribe(value => {
      let users_in = value[0];
      let users_out = value[1];
      if(this.div != null){
        this.renderer.removeChild(this.container.nativeElement, this.div);
      }
      this.div = null;
      this.div = this.renderer.createElement('div');
      for(let i = 0; i < users_in; i++){
        let i = this.renderer.createElement('i');
        this.renderer.addClass(i, "material-icons");
        this.renderer.addClass(i, "as-versus-pop-up-accepted");
        let text = this.renderer.createText("person");
        this.renderer.appendChild(i, text);
        this.renderer.appendChild(this.div, i);
      }
      for(let i = 0; i < users_out; i++){
        let i = this.renderer.createElement('i');
        this.renderer.addClass(i, "material-icons");
        this.renderer.addClass(i, "as-versus-pop-up-not-accepted");
        let text = this.renderer.createText("person");
        this.renderer.appendChild(i, text);
        this.renderer.appendChild(this.div, i);
      }
    
      this.renderer.appendChild(this.container.nativeElement, this.div);
    })
    this.who_acepted = 0;
    this.playAudio();
  }

  playAudio(){
    let audio = new Audio();
    audio.src = "/assets/sounds/door-knock.ogg";
    audio.volume = 0.7;
    audio.load();
    audio.play();
  }

  aceptMatch() {
    if(!this.pressed) {
      this.pressed = true;
      this.clicked.next(true);
    }
  }
  
}
