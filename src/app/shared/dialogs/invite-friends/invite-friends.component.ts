import { Component, OnInit, Inject } from '@angular/core';
import { MDC_DIALOG_DATA, MdcDialogRef } from '@angular-mdc/web';
import { Observable, Subject, empty } from 'rxjs';
import { map } from 'rxjs/operators';
import { VersusQuery } from 'src/app/dashboard/versus/state';

@Component({
  selector: 'app-invite-friends',
  templateUrl: './invite-friends.component.html',
  styleUrls: ['./invite-friends.component.scss']
})
export class InviteFriendsComponent implements OnInit{

  friends: any;
  friendClicked: Subject<any>;
  empty: boolean;
  //Lo mismo que askert service!
  constructor(
    private dialogRef: MdcDialogRef<InviteFriendsComponent>,
    private versusQuery: VersusQuery,
    @Inject(MDC_DIALOG_DATA) data: any
  ) {
    this.friends = [];
    this.empty = false;
    this.versusQuery.team$.subscribe(team => {
      if (team == null){
        this.friends = data.payload;
      } else {
        this.friends = [];
        for (let i = 0; i < data.payload.length; i++) {
          let exist = false;
          for (let j = 0; j < team.team.length; j++) {
            if (data.payload[i].friend_username == team.team[j].username || team.leader.username == data.payload[i].friend_username) {
              exist = true;
            }
          }
          if (exist == false) {
            this.friends.push(data.payload[i]);
          }
        }
        if (this.friends.length == 0) {
          this.empty = true;
        }
      }
    });
    this.friendClicked = data.friend_id;
  }

  ngOnInit() {
    
  }

  inviteFriend(id){
    this.friendClicked.next(id);
  }

}
