import { Component, OnInit, Inject} from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { MDC_DIALOG_DATA, MdcDialogRef, MdcSnackbar } from '@angular-mdc/web';
import { ApiService } from '../../services/api.service';
import { SessionService } from 'src/app/dashboard/state/session';

@Component({
  selector: 'app-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.scss']
})
export class ImageCropperComponent implements OnInit {

  imageChangedEvent: any = '';
  croppedImage: any = '';
  showCropper = false;

  constructor(
    @Inject(MDC_DIALOG_DATA) data: any,
    private snackbar: MdcSnackbar,
    private dialogRef: MdcDialogRef<ImageCropperComponent>,
    private sessionService: SessionService,
    private api: ApiService
  ) { 
    this.imageChangedEvent = data.event;
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  changeProfileImage(){
    this.doneProfileImage();
  }

  doneProfileImage(){
    console.log(this.croppedImage);
    this.api.changeImageProfile(this.croppedImage.split(',')[1]).subscribe(res => {
      let url = res["url"];
      let error = res["error"];
      if (error == undefined) {
        this.sessionService.changeUrlProfile(url);
        window.location.reload();
      }
    },
      error => {
        this.snackbar.open(`La imagen es demasiado grande (1.5 MB) o no tiene extensión correcta (jpg/png).`);
      
    });
    this.dialogRef.close('Iamge-dialog Closed');
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    this.showCropper = true;  
  }
  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }

}
