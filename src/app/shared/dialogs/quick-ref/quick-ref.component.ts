import { Component, OnInit } from '@angular/core';
import { MdcDialog, MdcDialogRef } from '@angular-mdc/web';

@Component({
  selector: 'app-quick-ref',
  templateUrl: './quick-ref.component.html',
  styleUrls: ['./quick-ref.component.scss']
})
export class QuickRefComponent implements OnInit {

  constructor(
    private dialogRef: MdcDialogRef<QuickRefComponent>
  ) { }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
