import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdcDialogModule, MdcImageListModule, MdcButtonModule, MdcCardModule, MdcTypographyModule, MdcListModule, MdcElevationModule, MdcIconModule, MdcSnackbarModule } from '@angular-mdc/web';
import { AskherComponent } from './askher/askher.component';
import { InviteFriendsComponent } from './invite-friends/invite-friends.component';
import { QuestionaryComponent } from './questionary/questionary.component';
import { ImageCropperComponent } from './image-cropper/image-cropper.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { QuickRefComponent } from './quick-ref/quick-ref.component';

@NgModule({
  imports: [
    CommonModule,
    MdcDialogModule,
    MdcImageListModule,
    MdcButtonModule,
    MdcCardModule,
    MdcTypographyModule,
    MdcListModule,
    MdcElevationModule,
    MdcIconModule,
    ImageCropperModule,
    MdcSnackbarModule
  ],
  exports: [AskherComponent, InviteFriendsComponent, QuestionaryComponent, ImageCropperComponent, QuickRefComponent],
  entryComponents: [AskherComponent, InviteFriendsComponent, QuestionaryComponent, ImageCropperComponent, QuickRefComponent],
  declarations: [AskherComponent, InviteFriendsComponent, QuestionaryComponent, ImageCropperComponent, QuickRefComponent]
})
export class DialogsModule { }
