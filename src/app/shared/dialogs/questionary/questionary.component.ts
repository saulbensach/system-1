import { Component, OnInit, Inject } from '@angular/core';
import { DialogsService, DialogsQuery } from '../state';
import { MDC_DIALOG_DATA, MdcDialogRef } from '@angular-mdc/web';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-questionary',
  templateUrl: './questionary.component.html',
  styleUrls: ['./questionary.component.scss']
})
export class QuestionaryComponent implements OnInit {

  answer: Subject<any>;
  question: any;
  
  constructor(
    private dialogRef: MdcDialogRef<QuestionaryComponent>,
    @Inject(MDC_DIALOG_DATA) payload: any
  ) { 
    this.question = payload.question;
    this.answer = payload.result;
  }

  ngOnInit() {
  }

  sendYes(){
    this.answer.next("yes");
    this.dialogRef.close();
  }

  sendNo(){
    this.answer.next("no");
    this.dialogRef.close();
  }

}
