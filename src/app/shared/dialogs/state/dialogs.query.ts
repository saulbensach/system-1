import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { DialogsStore } from './dialogs.store';
import { DialogsState } from './dialogs.model';

@Injectable({ providedIn: 'root' })
export class DialogsQuery extends Query<DialogsState> {

  opened$ = this.select(state => state.opened);
  type$ = this.select(state => state.type);
  askherType$ = this.select(state => state.askherType);

  constructor(protected store: DialogsStore) {
    super(store);
  }

}
