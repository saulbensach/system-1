import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { DialogsState } from './dialogs.model';
import * as storage from './../../../storage';

export function createInitialState(): DialogsState {
  return {
    opened: false,
    type: '',
    askherType: '',
    ...storage.getDialog()
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'dialogs' })
export class DialogsStore extends Store<DialogsState> {

  constructor() {
    super(createInitialState());
  }

  open(type: string, askherType?: string) {
    let dialog = storage.getDialog();
    dialog.opened = true;
    if(askherType != undefined || askherType != null){
      dialog.askherType = askherType;
    }
    dialog.type = type;
    this.updateDialog(dialog);
  }

  close() {
    let dialog = storage.getDialog();
    dialog.opened = false;
    dialog.askherType = '';
    dialog.type = '';
    this.updateDialog(dialog);
  }

  updateDialog(dialog: DialogsState) {
    this.update(dialog);
    storage.saveDialog(dialog);
  }

}

