import { Injectable } from "@angular/core";
import { MdcDialog } from "@angular-mdc/web";
import { DialogsStore } from "./dialogs.store";
import { DialogsQuery } from "./dialogs.query";
import { Subscription } from "rxjs";
import { AskherComponent } from "../askher/askher.component";
import { InviteFriendsComponent } from "../invite-friends/invite-friends.component";
import { QuestionaryComponent } from "../questionary/questionary.component";
import { ImageCropperComponent } from "../image-cropper/image-cropper.component";
import { VersusQuery } from "src/app/dashboard/versus/state";
import { DashboardQuery } from "src/app/dashboard/state/dashboard";

/**
 * Para añadir nuevos dialogos primero necesitamos crear un componente, declararlo en dialogs.module
 * y añadirlo tambíen en entrypoints
 * Una vez creado muy importante usar 
 * <mdc-dialog>
    <mdc-dialog-container>
        <mdc-dialog-surface>
    Respectivamente para que se renderice el dialogo
    Para abrir nuestro dialogo le tenemos que llamar por un tipo el que os de la gana
    añadir el nuevo caso en el switch que hay debajo
    para abrir nuestro dialogo tenemos que llamar al método openDialog que hay en este fichero
    finalmente le pasamos el tipo por el método y se abrirá el que toca.
    NOTA, el método está "sobrecargado" para añadir el tipo del askher
 */


@Injectable({providedIn: 'root'})
export class DialogsService {

    subscription: Subscription;
    subscriptionAsk: Subscription;
    question: any;

    constructor(
        private dialogQuery: DialogsQuery,
        private dialogStore: DialogsStore,
        private dialog: MdcDialog
    ) {
        this.dialogQuery.opened$.subscribe(opened => {
            if(opened) {
               this.subscription = this.dialogQuery.type$.subscribe(type => {
                   switch(type) {
                       case "image-cropper": {
                           let dialogRef = this.dialog.open(ImageCropperComponent, {
                                escapeToClose: true,
                                clickOutsideToClose: true,
                                buttonsStacked: false,
                                id: "image-cropper"
                           });
                           dialogRef.afterClosed().subscribe(() => {
                            this.dialogStore.close();
                           });
                           break;
                       }
                       case "askher": {
                           this.subscriptionAsk = this.dialogQuery.askherType$.subscribe(askherType => {
                            this.dialog.open(AskherComponent, {
                                data: {askType: askherType},
                                escapeToClose: false,
                                clickOutsideToClose: false,
                                buttonsStacked: false,
                                id: "ask-dialog"
                            });
                           });
                           break;
                       }
                   };
                });
            } else {
                this.dialog.closeAll();
                
                if(this.subscriptionAsk != undefined){
                    this.subscriptionAsk.unsubscribe();
                }
                if(this.subscription != undefined){
                    this.subscription.unsubscribe();
                }
                
            }
        });
    }

    getInviteDialog() {
        return this.dialog.getDialogById("invite-friends");
    }

    getAskherType(){
        return this.dialogQuery.askherType$;
    }

    openDialog(type: string, askherType?: string) {
        this.dialogStore.open(type, askherType);
    }

    closeDialog(){
        this.dialogStore.close();
    }

}