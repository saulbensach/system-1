export interface DialogsState {
    opened: boolean;
    type: string;
    askherType: string;
}