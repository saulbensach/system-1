import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from './layouts/layout.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    CommonModule, 
    RouterModule,
    LayoutModule
  ],
  declarations: []
})
export class ArenastackModule { }
