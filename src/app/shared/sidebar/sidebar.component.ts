import { Component, OnInit } from '@angular/core';
import { SessionQuery } from 'src/app/dashboard/state/session';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  //Si se quiere usar esta lista para el dia que vayan todos estos enlaces, hay que descomentar de la linea 20 a la 24 en el sidebar.component.html
  destinations = [
    {label: 'Home', icon: 'home', url: 'home'},
    {label: 'Versus', icon: 'compare_arrows', url: 'versus'},
    {label: 'Profile', icon: 'account_circle', url: 'profile'},
    {label: 'Wallet', icon: 'account_balance_wallet', url: 'wallet'},
    {label: 'Challenges', icon: 'whatshot', url: 'challenges'},
    {label: 'Arena Leaders', icon: 'star', url: 'arenaleader'},
    {label: 'Teams', icon: 'group', url: 'teams'},
    {label: 'Chat Room', icon: 'chat', url: 'chat-room'},
    {label: 'Events', icon: 'event', url: 'events'},
    {label: 'Help Center', icon: 'help', url: 'help'},
    {label: 'Settings', icon: 'settings', url: 'settings'}
  ]

  userName: Observable<any>;

  constructor(
    private sessionQuery: SessionQuery,
    private router: Router
  ) { }

  profileRoute() {
    this.userName.subscribe(username => {
      this.router.navigate(['profile', username])
    });
  }
  
  ngOnInit() {
    this.userName = this.sessionQuery.username$;
    this.userName = this.sessionQuery.username$;
  }

}
