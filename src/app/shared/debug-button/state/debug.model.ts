export interface DebugState {
    login: boolean;
    lobby: boolean;
    gameMode: string;
    lobbyPhase: string;
}