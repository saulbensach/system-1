import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { DebugState } from './debug.model';
import * as storage from '../../../storage';

export function createInitialState(): DebugState {
  return {
    login: false,
    lobby: false,
    gameMode: '',
    lobbyPhase: '',
    ...storage.getDebug()
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'debug' })
export class DebugStore extends Store<DebugState> {

  constructor() {
    super(createInitialState());
  }

  stateChange(state: DebugState) {
    this.update(state)
    storage.saveDebug(state);
  }

}

