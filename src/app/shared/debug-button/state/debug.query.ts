import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { DebugStore} from './debug.store';
import { DebugState } from './debug.model';

@Injectable({ providedIn: 'root' })
export class DebugQuery extends Query<DebugState> {

  login$ = this.select(state => state.login);
  mode$ = this.select(state => state.lobby);
  gameMode$ = this.select(state => state.gameMode);
  state$ = this.getSnapshot();

  constructor(protected store: DebugStore) {
    super(store);
  }

}
