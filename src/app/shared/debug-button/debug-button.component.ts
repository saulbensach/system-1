import { Component, OnInit, AfterContentInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { SessionStore, SessionService, SessionQuery } from 'src/app/dashboard/state/session';
import { LobbyService, LobbyStore } from 'src/app/dashboard/versus/lobby/state';
import { VersusStore } from 'src/app/dashboard/versus/state';
import { DebugStore, DebugQuery } from './state';
import { Observable } from 'rxjs';
import { DialogsService } from '../dialogs/state';
import { ToastrService, GlobalConfig } from 'ngx-toastr';
import { Friend } from 'src/app/models/friend.model';
import { FriendRequestComponent } from '../customs-toastrs/friend-request/friend-request.component';
import { ApiService } from '../services/api.service';
import { SessionState } from 'src/app/dashboard/state/session/session.model';

@Component({
  selector: 'app-debug-button',
  templateUrl: './debug-button.component.html',
  styleUrls: ['./debug-button.component.scss']
})
export class DebugButtonComponent implements OnInit, AfterContentInit {

  senpai: FormGroup;
  opened: boolean;
  options: GlobalConfig;
  barChosen: string;
  is_logged: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private sessionQuery: SessionQuery,
    private sessionService: SessionService,
    private lobbyStore: LobbyStore,
    private debugQuery: DebugQuery,
    private lobbyService: LobbyService,
    private versusStore: VersusStore,
    private debugStore: DebugStore,
    private toastr: ToastrService,
    private dialogService: DialogsService,
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.sessionQuery.isLoggedIn$.subscribe(value => {
      this.is_logged = value;
    })
    this.options = this.toastr.toastrConfig;
    const state = this.debugQuery.state$;
    this.opened = false;
    this.barChosen = "session"
    this.senpai = this.formBuilder.group({
      login: [state.login],
      lobby: [state.lobby],
      lobby_type: new FormControl(state.gameMode),
      lobby_phase: new FormControl(state.lobbyPhase)
    });
  }

  enterLobby() {
    this.lobbyService.createFalseLobby('solo');
    this.lobbyService.test2();
  }

  notification(){
    this.options.toastComponent = FriendRequestComponent;
    this.options.timeOut = 0;
    this.options.tapToDismiss = false;
    this.options.extendedTimeOut = 0;
    this.options.enableHtml = false;
    this.toastr.show('', ``);
  }

  chooseYuno(value: string) {
    this.barChosen = value;
  }

  logOut() {
    this.sessionService.logout();
  }

  ngAfterContentInit() {
    this.onChanges();
  }

  testDialog(value: string, askherType?: string){
    this.dialogService.openDialog(value, askherType);
  }

  closeDialog() {
    this.dialogService.closeDialog();
  }

  log_in(){
    console.log("looser!");
    this.apiService.login({email: 'saul@arenastack.com', password: 'Holatete123'}).subscribe(
      (session: SessionState) => {
        this.sessionService.login(session);
      },
      error => {
        const session = {
          token: "tuputamadre",
          name: "John",
          username: "paquito",
          surname: "Doe",
          picture: null,
          banner: null,
          friends: null,
          notifications: null,
          id: -1,
          steamid: "tuputamadre",
          balance: "9999"
        }
        this.sessionService.login(session);
        console.log("check if server is down!");

      }
    )
  }

  onChanges(): void {
   this.senpai.valueChanges.subscribe(value => {
      const debug = {
        login: value.login,
        lobby: value.lobby,
        gameMode: value.lobby_type,
        lobbyPhase: value.lobby_phase
      }
      this.debugStore.stateChange(debug);
      if(value.login){
        this.apiService.login({email: 'saul@arenastack.com', password: 'Holatete123'})
      } else {
        this.sessionService.logout();
      }
      if(value.lobby && value.lobby_type != "") {
        this.lobbyService.createFalseLobby(value.lobby_type);
        if(value.lobby_phase == "map-select"){
          this.lobbyStore.set_map("de_dust2");
        } else if(value.lobby_phase == "info"){
          //this.lobbyStore.set_map("");
        } else if(value.lobby_phase == "result") {
          const result = {
            winner: "Yomismo",
            team1score: "16",
            team2score: "14"
          }
          this.lobbyStore.matchEnded(result);
        }
      } else if(!value.lobby) {
        this.lobbyService.leaveLobby();
      }
      
    });
    
  }

  exitLobby() {
    this.lobbyService.leaveLobby()
  }

  test_wannaPlay() {
   if(!this.opened) {
      this.versusStore.false_wanna_play();
      this.opened = true;
    } else {
      this.opened = false;
      this.versusStore.closeShit();
    }
  }

}
