import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { AuthLayoutComponent } from './auth-layout/auth-layout.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MdcTopAppBarModule, MdcButtonModule, MdcDrawerModule, 
MdcListModule, MdcIconModule, MdcIconButtonModule, MdcTextFieldModule,
MdcElevationModule, MdcMenuModule, MdcTabBarModule, MdcMenuSurfaceModule, MdcSnackbarModule, MdcFormFieldModule, MdcSwitchModule, MdcRadioModule } from '@angular-mdc/web';
import { DirectivesModule } from '../directives/directives.module';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { DashboardNavbarComponent } from '../dash-nav/dashboard-navar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../dialogs/dialogs.module';
import { DebugButtonComponent } from '../debug-button/debug-button.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    MdcTopAppBarModule,
    MdcButtonModule,
    DirectivesModule,
    MdcListModule,
    MdcDrawerModule,
    MdcIconModule,
    MdcIconButtonModule,
    MdcTextFieldModule,
    MdcElevationModule,
    MdcMenuModule,
    MdcTabBarModule,
    MdcMenuSurfaceModule,
    MdcSnackbarModule,
    MdcMenuSurfaceModule,
    MdcFormFieldModule,
    MdcSwitchModule,
    ReactiveFormsModule,
    MdcRadioModule,
    DialogsModule
  ],
  declarations: [
    MainLayoutComponent,
    AuthLayoutComponent,
    SidebarComponent,
    DashboardNavbarComponent,
    DebugButtonComponent
  ]
})
export class LayoutModule { }
