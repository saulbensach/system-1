import { Component, OnInit } from '@angular/core';
import { Toast, ToastPackage, ToastrService } from 'ngx-toastr';
import {
  animate,
  keyframes,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import { DashboardService } from 'src/app/dashboard/state/dashboard';
import { InviteToastrQuery } from './state';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('inactive', style({
        opacity: 0,
      })),
      transition('inactive => active', animate('400ms ease-out', keyframes([
        style({
          transform: 'translate3d(100%, 0, 0) skewX(-30deg)',
          opacity: 0,
        }),
        style({
          transform: 'skewX(20deg)',
          opacity: 1,
        }),
        style({
          transform: 'skewX(-5deg)',
          opacity: 1,
        }),
        style({
          transform: 'none',
          opacity: 1,
        }),
      ]))),
      transition('active => removed', animate('400ms ease-out', keyframes([
        style({
          opacity: 1,
        }),
        style({
          transform: 'translate3d(100%, 0, 0) skewX(30deg)',
          opacity: 0,
        }),
      ]))),
    ]),
  ]
})
export class InviteComponent extends Toast {

  clicked: string;

  constructor(
    protected toastrService: ToastrService,
    public toastPackage: ToastPackage,
    private toastQuery: InviteToastrQuery
  ) { 
    super(toastrService, toastPackage);
    this.clicked = "";
    this.toastQuery.id$.subscribe(id => {
      if(id != -1){
        
      }
    })
  }

  accept(event: Event){
    
    event.stopPropagation();
    this.clicked = "accept";
    const action = {action: this.clicked, data: this.toastQuery.getSnapshot()};
    this.toastPackage.triggerAction(action);
    this.toastPackage.toastRef.close();
    return false;
  }

  decline(){
    
    this.toastPackage.toastRef.close();
  }
}
