import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { InviteToastrStore, InviteToastrState } from './invite-toastr.store';

@Injectable({ providedIn: 'root' })
export class InviteToastrQuery extends Query<InviteToastrState> {

  id$ = this.select(state => state.id);  

  constructor(protected store: InviteToastrStore) {
    super(store);
  }

}
