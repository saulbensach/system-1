import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { NullTemplateVisitor } from '@angular/compiler';

export interface InviteToastrState {
  id: number;
  leader_id: number;
  picture: string;
  username: string;
}

export function createInitialState(): InviteToastrState {
  return {
    id: -1,
    leader_id: -1,
    picture: null,
    username: null
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'inviteToastr' })
export class InviteToastrStore extends Store<InviteToastrState> {

  constructor() {
    super(createInitialState());
  }

  saveToastr(id, leader_id, picture, username){
    let toastr: InviteToastrState = {
      id: id,
      leader_id: leader_id,
      picture: picture,
      username: username
    }
    this.update(toastr);
  }

}

