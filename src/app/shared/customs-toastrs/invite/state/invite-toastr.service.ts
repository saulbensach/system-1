import { Injectable } from '@angular/core';
import { InviteToastrStore } from './invite-toastr.store';

@Injectable({ providedIn: 'root' })
export class InviteToastrService {

  constructor(
    private toastStore: InviteToastrStore
  ) {
  }

  createToastr(id, leader_id, picture, username){
    this.toastStore.saveToastr(id, leader_id, picture, username);
  }

}
