import { Component, OnInit } from '@angular/core';
import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';
import {
  animate,
  keyframes,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import { InviteToastrQuery } from '../invite/state';

@Component({
  selector: 'app-friend-request',
  templateUrl: './friend-request.component.html',
  styleUrls: ['./friend-request.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('inactive', style({
        opacity: 0,
      })),
      transition('inactive => active', animate('400ms ease-out', keyframes([
        style({
          transform: 'translate3d(100%, 0, 0) skewX(-30deg)',
          opacity: 0,
        }),
        style({
          transform: 'skewX(20deg)',
          opacity: 1,
        }),
        style({
          transform: 'skewX(-5deg)',
          opacity: 1,
        }),
        style({
          transform: 'none',
          opacity: 1,
        }),
      ]))),
      transition('active => removed', animate('400ms ease-out', keyframes([
        style({
          opacity: 1,
        }),
        style({
          transform: 'translate3d(100%, 0, 0) skewX(30deg)',
          opacity: 0,
        }),
      ]))),
    ]),
  ]
})
export class FriendRequestComponent extends Toast {

  public user: string;
  public avatar: string;

  constructor(
    protected toastrService: ToastrService,
    public toastPackage: ToastPackage
  ) { 
    super(toastrService, toastPackage);
    let s = toastPackage.message.toString();
    if(s == null || s == '' || s.length == 0){
      this.avatar = "/assets/img/dashboard/default_avatar.png";
      this.user = "pollo69";
    } else {
      let payload = JSON.parse(s);
      this.avatar = payload.profile;
      this.user = payload.friend_username;
    }
    
  }

}
