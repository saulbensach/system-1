import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleComponent } from './simple/simple.component';
import { MdcSnackbarModule } from '@angular-mdc/web';

@NgModule({
  imports: [
    CommonModule,
    MdcSnackbarModule
  ],
  declarations: [SimpleComponent]
})
export class SnackbarsModule { }
