import { Routes, RouterModule } from "@angular/router";

export const routes:Routes = [
    {
        path: 'login',
        loadChildren: './login/login.module#LoginModule'
    },
    {
        path: 'register',
        loadChildren: './register/register.module#RegisterModule'
    },
    {
        path: 'forgot',
        loadChildren: './forgot/forgot.module#ForgotModule'
    },
    {
        path: 'renew_password',
        loadChildren: './renew-password/renew-password.module#RenewPasswordModule'
    }
];

export const routing = RouterModule.forChild(routes);