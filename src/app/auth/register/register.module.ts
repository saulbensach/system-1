import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { MdcCardModule, MdcListModule, MdcTextFieldModule, MdcButtonModule, MdcIconModule, MdcIconButtonModule, MdcElevationModule, 
  MdcFormFieldModule, MdcCheckboxModule, MdcSelectModule } from '@angular-mdc/web';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RegisterRoutingModule,
    MdcCardModule,
    MdcListModule,
    MdcTextFieldModule,
    MdcButtonModule,
    ReactiveFormsModule,
    MdcIconModule,
    MdcIconButtonModule,
    MdcElevationModule,
    MdcFormFieldModule,
    FormsModule,
    MdcCheckboxModule,
    MdcSelectModule
  ],
  declarations: [RegisterComponent]
})
export class RegisterModule { }
