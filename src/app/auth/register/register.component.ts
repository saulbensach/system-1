import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from 'src/app/dashboard/state/session';
import { ApiService } from 'src/app/shared/services/api.service';
import { SessionState } from 'src/app/dashboard/state/session/session.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  partnerCode: string;
  registerForm: FormGroup;
  username_taken: boolean;
  email_taken: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private api: ApiService,
    private sessionService: SessionService
  ) { 
    this.route.queryParams.subscribe(params => {
      this.partnerCode = params["partner"];
    })
  }

  ngOnInit() {
    this.email_taken = false;
    this.username_taken = false;
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])],
      surname: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])],
      birthday: [
        '', 
        Validators.compose(
          [
            Validators.required,
            Validators.pattern(new RegExp("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))"))
          ]
        )
      ],
      gender: ['', Validators.required],
      currency: ['', Validators.required],
      country: ['', Validators.required],
      username: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])],
      email: [
        '', 
        Validators.compose(
          [
            Validators.required, 
            Validators.pattern(new RegExp('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$')),
            Validators.minLength(5)
          ]
        )
      ],
      password: [
        '', 
        Validators.compose(
          [
            Validators.minLength(8), 
            Validators.required, 
            Validators.pattern(new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).*"))
          ]
        )
      ],
      terms_of_service: ['', Validators.required]
    });
  }

  get name() {return this.registerForm.get('name');}
  get surname() {return this.registerForm.get('surname');}
  get password() {return this.registerForm.get('password');}
  get birthday() {return this.registerForm.get('birthday');}
  get gender() {return this.registerForm.get('gender');}
  get currency() {return this.registerForm.get('currency');}
  get country() {return this.registerForm.get('country');}
  get username() {return this.registerForm.get('username');}
  get email() {return this.registerForm.get('email');}
  get terms() {return this.registerForm.get('terms_of_service');}

  resetUsername(){
    this.username_taken = false;
  }

  resetEmail(){ this.email_taken = false; }

  onSubmit() {
    if(this.registerForm.valid) {
      this.api.register(this.registerForm.value, this.partnerCode).subscribe(
        (session: SessionState) => {
          this.sessionService.register(session);
          this.router.navigate(['/']);
        },
        error => { 
          let message = error.error.message.error;
          this.username_taken = false;
          this.email_taken = false;
          if(message.email != null){
            this.email_taken = true;
          }else if(message.username != null){
            this.username_taken = true;
          } else if(message.email_username != null){
            this.username_taken = true;
            this.email_taken = true;
          }
        }
      );
    }
    
  }

}
