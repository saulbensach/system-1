import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { routing } from './auth.routing';
import { MdcSnackbarModule } from '@angular-mdc/web';

@NgModule({
  imports: [
    CommonModule,
    routing,
    MdcSnackbarModule
  ],
  declarations: [AuthComponent]
})
export class AuthModule { }
