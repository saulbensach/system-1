import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RenewPasswordComponent } from "./renew-password.component";

const routes: Routes = [{
    path: ':token',
    component: RenewPasswordComponent
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
    providers: []
})

export class RenewPasswordRoutingModule {}