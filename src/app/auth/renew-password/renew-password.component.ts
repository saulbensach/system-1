import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ApiService } from 'src/app/shared/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MdcSnackbar } from '@angular-mdc/web';

@Component({
  selector: 'app-renew-password',
  templateUrl: './renew-password.component.html',
  styleUrls: ['./renew-password.component.scss']
})
export class RenewPasswordComponent implements OnInit {

  renewpasswordForm: FormGroup;
  same_password: boolean;

  constructor(
    private api: ApiService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private snackbar: MdcSnackbar
  ) { }

  ngOnInit() {
    this.same_password = true;
    this.renewpasswordForm = this.formBuilder.group({
      password: [
        '', 
        Validators.compose(
          [
            Validators.minLength(8), 
            Validators.required, 
            Validators.pattern(new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).*"))
          ]
        )
      ],
      password2: [
        '', 
        Validators.compose(
          [
            Validators.minLength(8), 
            Validators.required, 
            Validators.pattern(new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).*"))
          ]
        )
      ]
    });
  }

  get form() {return this.renewpasswordForm.controls;}

  get password() {return this.renewpasswordForm.get('password');}
  get password2() {return this.renewpasswordForm.get('password2');}

  reset(){
    this.same_password = true;
  }

  onSubmit() {
    if (this.renewpasswordForm.value.password != this.renewpasswordForm.value.password2){
      this.same_password = false;
    } else if (this.route.snapshot.params['token'] != null && this.route.snapshot.params['token'] != ""){
      this.same_password = true;
      let tries = 5;
      this.api.sendTokenNewPassword(this.route.snapshot.params['token'], this.renewpasswordForm.value.password).subscribe(res => {
        this.snackbar.open(`Tu contraseña se ha cambiado satisfactoriamente, estás siendo redirigido.`);
        let interval = setInterval(() => {
          if (tries == 0){
            this.router.navigate(['/auth/login'])
            this.snackbar.dismiss();
            clearInterval(interval);
          } else {
            tries--;
          }
        }, 500);
      });
    }
  }

}
