import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RenewPasswordRoutingModule } from './renew-password-routing.module';
import { RenewPasswordComponent } from './renew-password.component';
import { MdcCardModule, MdcListModule, MdcTextFieldModule, MdcButtonModule, MdcIconModule, MdcIconButtonModule, MdcElevationModule, 
  MdcFormFieldModule, MdcCheckboxModule, MdcSelectModule } from '@angular-mdc/web';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RenewPasswordRoutingModule,
    MdcCardModule,
    MdcListModule,
    MdcTextFieldModule,
    MdcButtonModule,
    ReactiveFormsModule,
    MdcIconModule,
    MdcIconButtonModule,
    MdcElevationModule,
    MdcFormFieldModule,
    FormsModule,
    MdcCheckboxModule,
    MdcSelectModule
  ],
  declarations: [RenewPasswordComponent]
})
export class RenewPasswordModule { }
