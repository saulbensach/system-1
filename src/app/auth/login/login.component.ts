import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/dashboard/state/session';
import { ApiService } from 'src/app/shared/services/api.service';
import { SessionState } from 'src/app/dashboard/state/session/session.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  validCredentials: boolean;
  sended: boolean;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder, 
    private api: ApiService,
    private sessionService: SessionService
    ) { }

  ngOnInit() {
    this.sended = false;
    this.validCredentials = true;
    this.loginForm = this.formBuilder.group({
      email: [
        '', 
        Validators.compose(
          [
            Validators.required, 
            Validators.pattern(new RegExp('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$')),
            Validators.minLength(5)
          ]
        )
      ],
      password: [
        '', 
        Validators.compose(
          [
            Validators.minLength(8), 
            Validators.required, 
            Validators.pattern(new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).*"))
          ]
          )
        ]
    });
  }

  get form() {return this.loginForm.controls;}

  get email() {return this.loginForm.get('email');}
  get password() {return this.loginForm.get('password');}

  reset(){
    this.validCredentials = true;
  }

  onSubmit() {
    //console.log(this.loginForm);
    if(this.loginForm.valid){
      this.sended = true;
      this.validCredentials = true;
      this.api.login(this.loginForm.value).subscribe(
        (session: SessionState) => {
          this.sessionService.login(session);
          this.router.navigate(['/']);
        },
        error => {this.validCredentials = false;}
      );
    }
  }

}
