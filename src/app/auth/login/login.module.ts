import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { MdcCardModule, MdcListModule, MdcTextFieldModule, MdcButtonModule, MdcIconModule, MdcIconButtonModule, MdcElevationModule, 
  MdcFormFieldModule, MdcCheckboxModule } from '@angular-mdc/web';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    MdcCardModule,
    MdcListModule,
    MdcTextFieldModule,
    MdcButtonModule,
    ReactiveFormsModule,
    MdcIconModule,
    MdcIconButtonModule,
    MdcElevationModule,
    MdcFormFieldModule,
    FormsModule,
    MdcCheckboxModule
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }
