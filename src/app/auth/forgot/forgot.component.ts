import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ApiService } from 'src/app/shared/services/api.service';
import { MdcSnackbar } from '@angular-mdc/web';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {

  forgotForm: FormGroup;

  constructor(
    private api: ApiService,
    private snackbar: MdcSnackbar,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.forgotForm = this.formBuilder.group({
      email: [
        '', 
        Validators.compose(
          [
            Validators.required, 
            Validators.pattern(new RegExp('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$')),
            Validators.minLength(5)
          ]
        )
      ]
    })
  }

  get email() {return this.forgotForm.get('email');}

  onSubmit() {
    if(this.forgotForm.valid) {
      this.api.sendResetEmail(this.forgotForm.value.email).subscribe(res => {
        this.snackbar.open('Email sended.')
      });
    }
  }

  sendEmailForgotPassword() {

  }

}
