import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotRoutingModule } from './forgot-routing.module';
import { ForgotComponent } from './forgot.component';
import { MdcListModule, MdcTextFieldModule, MdcButtonModule, MdcIconModule, MdcIconButtonModule, MdcElevationModule, 
  MdcFormFieldModule, MdcCheckboxModule } from '@angular-mdc/web';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ForgotRoutingModule,
    MdcListModule,
    MdcTextFieldModule,
    MdcButtonModule,
    ReactiveFormsModule,
    MdcIconModule,
    MdcIconButtonModule,
    MdcElevationModule,
    MdcFormFieldModule,
    FormsModule,
    MdcCheckboxModule
  ],
  declarations: [ForgotComponent]
})
export class ForgotModule { }
