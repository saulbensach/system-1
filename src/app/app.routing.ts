import {Routes, RouterModule} from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { MainLayoutComponent } from './shared/layouts/main-layout/main-layout.component';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';

// Como quiero dejar entrar al dashboard pongo el authguard es otros sitios concretos de la app
export const routes: Routes = [
    {
        path: '',
        component: MainLayoutComponent,
        children: [
            {
                path: '', 
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            }
        ]
    },
    {
        path: 'auth',
        component: AuthLayoutComponent,
        loadChildren: './auth/auth.module#AuthModule'
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {useHash: true});