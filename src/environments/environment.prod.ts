export const environmentProd = {
  production: true,
  base_url: 'http://81.202.101.216:4000/api/v1/',
  socket_url: 'wss://81.202.101.216:4000/socket',
  steam_auth: 'https://81.202.101.216:4000/auth/steam?token='
};
