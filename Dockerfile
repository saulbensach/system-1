FROM node:latest
RUN mkdir /webapp
COPY . /webapp
WORKDIR /webapp

RUN npm install -b @angular/cli
RUN npm install

CMD ng serve --host 0.0.0.0 -c production